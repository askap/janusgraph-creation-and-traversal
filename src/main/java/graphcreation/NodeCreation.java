package graphcreation;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import org.apache.commons.collections.ListUtils;
import org.janusgraph.core.JanusGraph;
import org.janusgraph.core.JanusGraphTransaction;
import org.janusgraph.core.JanusGraphVertex;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

/*
 * 	This module is the first step for creation of any graph  
	in watch tower.It helps us to create basic schema for our graph database 
	and also gives us ability manually define indexes on various properties of a node.
 
   
   	Attributes
   	----------
   	yaml_file : .yaml file
       Configuration file which contains all the 
       properties of a node , edge and all the other 
       data files and graph database location 
    
    data_file : .txt 
     	Contains the data to load into the graph.
       
   	Examples
   	--------
   	The loadData function will be called from 
    a separate wrapper as an object of NodeCreation class	:
   	
   	obj = loadData("data_file","test.yaml");
*/

public class NodeCreation {

    private long batchSize;
    private JanusGraph graph;
    private String dataFileName;
    private ArrayList<String> nodeKeyColumns;
    private int numberOfNodes;
    private ArrayList<String> nodePropertiesList;
    private List<String> listPropertiesInNode;

    public NodeCreation(JanusGraph graph, long batchSize, String dataFileName, ArrayList<String> nodeKeyColumns,
            List<Map<String, Object>> nodeProperties, List<String> listPropertiesInNode) {
        this.graph = graph;
        this.batchSize = batchSize;
        this.dataFileName = dataFileName;
        this.nodeKeyColumns = nodeKeyColumns;
        this.numberOfNodes = 0;
        this.nodePropertiesList = new ArrayList<String>();
        for (Map<String, Object> map : nodeProperties) {
            for (Map.Entry<String, Object> entry : map.entrySet()) {
                // Getting the propety name from nodeProperties settings
                this.nodePropertiesList.add(entry.getKey());
                // Getting the data type for the propety from nodeProperties settings
            }
        }
        this.listPropertiesInNode = listPropertiesInNode;
    }

    private void createNodes(){
        
        // Get the List of Non Key Node Properties to be inserted
        ArrayList<String> processedNodeKeyColumns = new ArrayList<String>();
        processedNodeKeyColumns.addAll((Collection<? extends String>) nodeKeyColumns);
        List<String> requiredPropertiesFromCSV = nodePropertiesList;
        requiredPropertiesFromCSV.add("key");
        requiredPropertiesFromCSV.add("kpi");
        requiredPropertiesFromCSV.add("parent_keys");

        List<String> nonKeyNodeProperties = ListUtils.subtract(nodePropertiesList, nodeKeyColumns);
        File f = new File(dataFileName);
        JanusGraphTransaction tnx = graph.newTransaction();
        try {
            LineIterator it = FileUtils.lineIterator(f, "UTF-8"); 
            int i = 0;
            String firstLine = it.nextLine();
            String[] columns = firstLine.split("\\|"); 
            String key;
            String kpi;
            String level;
            String time;
            String unprocessedLevelNum;
            int levelNumber;        
            String id;
            JanusGraphVertex v;
            String property;
            String informationFromfile;
            HashMap record = new HashMap<String, String>();
            String currentLine;
            String currentColumn;
            String currentValue;
            while (it.hasNext()){
                currentLine = it.nextLine();
                String[] values = currentLine.split("\\|");
                for (int valuePos = 0; valuePos < values.length; valuePos++) { 
                    currentColumn = columns[valuePos];
                    if(requiredPropertiesFromCSV.contains(currentColumn)){
                        currentValue = values[valuePos];
                        record.put(currentColumn, currentValue);
                    }
                }  
                i++;


                // getting the data of the key columns or helper columns though which key column
                // will be formed
                // manually as it is compulsory
                key = record.get("key").toString();
                kpi = record.get("kpi").toString();
                level = record.get("level").toString();
                time = "[" + record.get("time").toString() + "]";
                unprocessedLevelNum = record.get("level_num").toString();
                levelNumber = Integer.parseInt(unprocessedLevelNum);
                // property created by us
                id = kpi + "-*-" + key;
                // Creation of the node inside graph
                // Label of the nodes is KPI
                v = tnx.addVertex("KPI");
                // Inserting key properties manually since they are compulsory
                v.property("name", kpi);
                v.property("ID", id);
                v.property("level", level);
                v.property("time", time);
                v.property("level_num", levelNumber);

                // Automated the insertion of non key proerties inside the node
                for (int j = 0; j < nonKeyNodeProperties.size(); j++) {
                    try {
                        property = (String) nonKeyNodeProperties.get(j);
                        if(record.containsKey(property)){
                            informationFromfile = record.get(property).toString();
                            // Condition for list properties
                            if (listPropertiesInNode.contains(property)) {
                                informationFromfile = "[" + informationFromfile + "]";
                            }
                            v.property(property, informationFromfile);
                        }
                    } catch (IllegalArgumentException e) {
                        // If particular property is not configured in the schema
                        // But is present in the Data file... Dont insert the property
                        continue;
                    } catch (Exception e) {
                        // If Any other issue occurs, Dont insert the property
                        // But print error message for log
                        e.printStackTrace();
                        continue;
                    }
                }
                // Logic to commit the transaction to the graph backend ones the Batch Size is
                // reached
                // Post commiting we will create a new transaction to proceef futher
                if (i % batchSize == 0) {
                    tnx.commit();
                    tnx.close();
                    tnx = graph.newTransaction();
                    System.out.println("periodic commit at : " + i);
                }

            }
            if (tnx.isOpen()) {
                tnx.commit();
                tnx.close();
    
                System.out.println("Final commit at : " + i);
    
            }
            numberOfNodes = i;
        } catch(FileNotFoundException e){
            e.printStackTrace();
        } catch(IOException e){
            e.printStackTrace();
        }
    }

    public int initiateNodeCreation() {
        try {
            createNodes();
            return numberOfNodes;
        }

        catch (Exception e) {
            System.out.print("\n\n\n####################  ERROR  ##########################\n");
            System.out.print(e.getMessage() + "\n");
            e.printStackTrace();
            System.out.print("\n##############################################\n\n\n");
            return 0;
        }
    }

}
