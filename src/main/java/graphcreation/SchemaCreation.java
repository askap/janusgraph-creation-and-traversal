package graphcreation;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.tinkerpop.gremlin.structure.Vertex;
import org.janusgraph.core.JanusGraph;
import org.janusgraph.core.Multiplicity;
import org.janusgraph.core.PropertyKey;
import org.janusgraph.core.attribute.Geoshape;
import org.janusgraph.core.schema.JanusGraphIndex;
import org.janusgraph.core.schema.JanusGraphManagement;

/*
 
 This module is the first step for creation of any graph  
 in watch tower.It helps us to create basic schema for our graph database 
 and also gives us ability manually define indexes on various properties of a node.
  
    
    Attributes
    ----------
    yaml_file : .yaml file
        Configuration file which contains all the 
        properties of a node , edge and all the other 
        data files and graph db loacation 
        
    Examples
    --------
    #The SchemaCreation function will be called from 
     a separate wrapper as an object of SchemaCreation class	:
   
    obj = SchemaCreation("test.yaml");

 
 
 
 
  
 */
public class SchemaCreation {

	private JanusGraph graph;
	private List<Map<String, Object>> nodeProperties;
	private List<Map<String, Object>> bridgeProperties;
	private List<Map<String, Object>> causalProperties;
	private ArrayList<String> nodeCompositeIndexProperties;
	private JanusGraphManagement mgmt;
	private HashMap<String,Class> janusDataTypeMapping;

	

	public SchemaCreation(JanusGraph graph, List<Map<String, Object>> nodeProperties,List<Map<String, Object>> bridgeProperties,List<Map<String, Object>> causalProperties, ArrayList<String> nodeCompositeIndexProperties) {
		this.graph = graph;
		this.nodeProperties = nodeProperties;
		this.bridgeProperties = bridgeProperties;
		this.causalProperties = causalProperties;
		this.nodeCompositeIndexProperties = nodeCompositeIndexProperties;

		// Manual Assignment
		this.janusDataTypeMapping = new HashMap<String,Class>();
    	this.janusDataTypeMapping.put("String", String.class);
        this.janusDataTypeMapping.put("Character", Character.class);
        this.janusDataTypeMapping.put("Boolean", Boolean.class);
        this.janusDataTypeMapping.put("Byte", Byte.class);
        this.janusDataTypeMapping.put("Short", Short.class);
        this.janusDataTypeMapping.put("Integer", Integer.class);
        this.janusDataTypeMapping.put("Long", Long.class);
        this.janusDataTypeMapping.put("Float", Float.class);
        this.janusDataTypeMapping.put("Geoshape", Geoshape.class);
        this.janusDataTypeMapping.put("UUID", UUID.class);
		this.janusDataTypeMapping.put("Date", Date.class);
		
		//Opening the vortex to assign meta data for Janus Graph
		this.mgmt = graph.openManagement();
	}

	private void assignMetaDataForNodeProperties(){
		System.out.print("\n\n\n##############################################\n");
		System.out.print("Defining Schema of the Nodes");
		System.out.print("\n##############################################\n\n\n");
		//Defining vertex label
		mgmt.makeVertexLabel("KPI").make();
		//Defining the properties for the node
		for (Map<String, Object> map : nodeProperties) {
			for (Map.Entry<String, Object> entry : map.entrySet()) {
				// Getting the propety name from nodeProperties settings
				String key = entry.getKey();
				// Getting the data type for the propety from nodeProperties settings
				Object value = entry.getValue();
				value=value.toString();
				// Putting the nodes property into schema with appropriate data type supported by janus graph
				mgmt.makePropertyKey(key).dataType(janusDataTypeMapping.get(value)).make();
			}
		}
	}

	private void createCompositeIndexForNodeProperties() {
		System.out.print("\n\n\n##############################################\n");
		System.out.print("Defining Schema of the Nodes");
		System.out.print("\n##############################################\n\n\n");
		//Defining the index for the relevant node properties as provide in nodeCompositeIndexProperties settings
		for (int i = 0; i < nodeCompositeIndexProperties.size();i++) {
			// Identifying the property name from settings
			String property=nodeCompositeIndexProperties.get(i);
			// Getting the propety from the Graph Metadata defined for the Nodes earlier
			final PropertyKey prop = mgmt.getPropertyKey(property);
			// Creating the name of the index
			String keyProperty="by"+property+"Composite";
			// Building the index using Janus API
			JanusGraphManagement.IndexBuilder idIndexBuilder = mgmt.buildIndex(keyProperty, Vertex.class).addKey(prop);
			// Creating a Composite Index for the Property
			JanusGraphIndex idIndex = idIndexBuilder.buildCompositeIndex();
		}

	}

	private void assignMetaDataForBridgeProperties() {

		System.out.print("\n\n\n##############################################\n");
		System.out.print("Defining Schema of the Bridge edges");
		System.out.print("\n##############################################\n\n\n");
		// Defining Bridge edge label and its Multiplicity
		// Multiplicity of an edge label defines a multiplicity constraint on all edges of this label, 
		// that is, a maximum number of edges between pairs of vertices
		// SIMPLE: Allows at most one edge of such label between any pair of vertices. 
		// In other words, the graph is a simple graph with respect to the label. 
		// Ensures that edges are unique for a given label and pairs of vertices.
		// We cannot have multiple Bridge edges between a given pair of nodes
		mgmt.makeEdgeLabel("Bridge").multiplicity(Multiplicity.MULTI).make();
		//Defining the properties for the bridge edge
		for (Map<String, Object> map : bridgeProperties) {
			for (Map.Entry<String, Object> entry : map.entrySet()) {
				// Getting the propety name from bridgeProperties settings
				String key = entry.getKey();
				// Getting the data type for the propety from bridgeProperties settings
				Object value = entry.getValue();
				value=value.toString();
				// Putting the edge property into schema with appropriate data type supported by janus graph
				mgmt.makePropertyKey(key).dataType(janusDataTypeMapping.get(value)).make();
			}
		}
	}


	private void assignMetaDataForCausalProperties() {

		System.out.print("\n\n\n##############################################\n");
		System.out.print("Defining Schema of the Causal edges");
		System.out.print("\n##############################################\n\n\n");
		// Defining Causal edge label and its Multiplicity
		// Multiplicity of an edge label defines a multiplicity constraint on all edges of this label, 
		// that is, a maximum number of edges between pairs of vertices
		// MULTI: Allows multiple edges of the same label between any pair of vertices.
		// In other words, the graph is a multi graph with respect to such edge label. 
		// There is no constraint on edge multiplicity.
		// We can have multiple Causal edges between two vertices
		mgmt.makeEdgeLabel("Causal").multiplicity(Multiplicity.MULTI).make();
		//Defining the properties for the Causal edge
		for (Map<String, Object> map : causalProperties) {
			for (Map.Entry<String, Object> entry : map.entrySet()) {
				// Getting the propety name from casusalProperties settings
				String key = entry.getKey();
				// Getting the data type for the propety from causalProperties settings
				Object value = entry.getValue();
				value=value.toString();
				// Putting the edge property into schema with appropriate data type supported by janus graph
				mgmt.makePropertyKey(key).dataType(janusDataTypeMapping.get(value)).make();
			}
		}
	}

	public boolean createSchema(){
		try{
			// Create Meta Data for Nodes
			assignMetaDataForNodeProperties();
			// Create Index for Nodes
			createCompositeIndexForNodeProperties();
			// Create Meta Data for bridges
			assignMetaDataForBridgeProperties();
			// Create Meta Data for Causal edges
			assignMetaDataForCausalProperties();

			// Closing the vortex and commiting.
			mgmt.commit();
			// Commiting the changes to backend graph
			graph.tx().commit();

			mgmt = graph.openManagement();
			System.out.println("Printing Schema");
			System.out.println(mgmt.printSchema());
			mgmt.commit();

			return true;
		}
		catch (Exception e){
			// If any issues occurs, print error message and return false
			System.out.print("\n\n\n####################  ERROR  ##########################\n");
            System.out.print(e.getMessage()+"\n");
            e.printStackTrace();
            System.out.print("\n##############################################\n\n\n");
			return false;
		}
	}
    
}
