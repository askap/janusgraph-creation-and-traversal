package graphcreation;
import org.janusgraph.core.JanusGraph;
import org.janusgraph.core.JanusGraphFactory;
import app.GraphCreationDto;
import utils.YamlFileProcessorUtil;
import java.util.concurrent.Callable;
import java.util.Arrays;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/*
 Wrapper class which calls different modules of our graph creation.
 */
public class GraphCreation {

	private JanusGraph graph;
	private int numberOfNodes;
	private int numberOfBridgeEdges;
	private int numberOfCausalEdges;
	private String data_file;
	private String causal_file;
	private String db_property_file;
	YamlFileProcessorUtil yamlObj;

	public GraphCreation(GraphCreationDto graphConfig) {
		this.data_file = graphConfig.getData_file();
		this.causal_file = graphConfig.getCausal_file();
		this.db_property_file = graphConfig.getDb_property_file();
		yamlObj = new YamlFileProcessorUtil("/graph_config.yaml");
		}
	
	Callable<Void> callableBridgeCreation = new Callable<Void>()
    {
       @Override
       public Void call() throws Exception
       {
        try{
			BridgeCreation bridgeObj = new BridgeCreation(graph, yamlObj.getBatchSize(), data_file);
			numberOfBridgeEdges = bridgeObj.initiateBridgeCreation();
			graph.tx().rollback();
			return null;
        } catch (Exception e){
            System.out.println("Issue with Bridge Creation function in GraphCreation");
            return null;
            }
        }
	};
	
	Callable<Void> callableCausalCreation = new Callable<Void>()
    {
       @Override
       public Void call() throws Exception
       {
        try{
			CausalCreation causalObj = new CausalCreation(graph, yamlObj.getBatchSize(), causal_file);
			numberOfCausalEdges = causalObj.initiateCausalCreation();
			graph.tx().rollback();
			return null;
        } catch (Exception e){
            System.out.println("Issue with Causal Creation function in GraphCreation");
			return null;
            }
        }
    };

	private boolean[] graphCreation() {
		boolean processStatus;
		long startTime;
		long endTime;
		boolean[] creationStatus = { false, false, false, false };

		graph = JanusGraphFactory.open(this.db_property_file);
		startTime = System.currentTimeMillis();
		try {
			// Create Schema
			SchemaCreation schemaObj = new SchemaCreation(graph, yamlObj.getNodeProperties(),
					yamlObj.getBridgeProperties(), yamlObj.getCausalProperties(),
					yamlObj.getNodeCompositeIndexProperties());
			processStatus = schemaObj.createSchema();
			long schemaEndTime = System.currentTimeMillis();
			System.out.println("\n\n\n###########################\nTime for schema creation "
					+ (schemaEndTime - startTime) / 1000 + " seconds\n################\n\n");
			if (processStatus == false) {
				return creationStatus;
			} else {
				creationStatus[0] = true;
			}
			
			NodeCreation nodeObj = new NodeCreation(graph, yamlObj.getBatchSize(), this.data_file,
					yamlObj.getNodeKeyColumns(), yamlObj.getNodeProperties(), yamlObj.getListPropertiesInNode());

			numberOfNodes = nodeObj.initiateNodeCreation();
			if (numberOfNodes > 0) {
				processStatus = true;
			} else {
				processStatus = false;
			}
			if (processStatus == false) {
				return creationStatus;
			} else {
				creationStatus[1] = true;
			}
			graph.tx().rollback();
			long nodesEndTime = System.currentTimeMillis();
			System.out.println("\n\n\n###########################\nTime for Nodes creation: "
					+ (nodesEndTime - schemaEndTime) / 1000 + " seconds\n################\n\n");

			if (processStatus == false) {
				return creationStatus;
			} else {
				creationStatus[1] = true;
			}
			long bridgeStartTime = System.currentTimeMillis();
			ExecutorService executor = Executors.newFixedThreadPool(2);
			
			executor.invokeAll(Arrays.asList(callableBridgeCreation,callableCausalCreation));
			executor.shutdown();
            while (!executor.isTerminated()) {
            }
			
			if (numberOfBridgeEdges > 0) {
				creationStatus[2] = true;
			} 
			if (numberOfCausalEdges > 0) {
				creationStatus[3] = true;
			}
			
			long causalEndTime = System.currentTimeMillis();
			System.out.println("\n\n\n###########################\nTime for Causal creation: "
					+ (causalEndTime - bridgeStartTime) / 1000 + " seconds\n################\n\n");
			
		} catch (Exception e) {
			System.out.println("Issue in Graph Creation");
			e.printStackTrace();
		} 
		finally {
			// Closing the Graph
			graph.tx().rollback();
			graph.close();
			graph = null;

		}

		endTime = System.currentTimeMillis();

		System.out.println("\n\n\n###########################\nTotal Time for Graph Creation: "
				+ (endTime - startTime) / 1000 + " seconds\n################\n\n");

		return creationStatus;

	}

	public int getNumberOFNodes() {
		return numberOfNodes;
	}

	public int getNumberOFBridgeEdges() {
		return numberOfBridgeEdges;
	}

	public int getNumberOFCausalEdges() {
		return numberOfCausalEdges;
	}

	public boolean initiateCreateGraph() {

		boolean[] creationStatus = graphCreation();
		if (creationStatus[0] == false || creationStatus[1] == false) {
			System.out.println("Graph Not Created");
			return false;
		} else if (creationStatus[2] == false && creationStatus[3] == false) {
			System.out.println(
					"Graph Created Without Bridges and Causal Edges, only Anomaly Report possible, No RCA or Impact will be supported");
			return false;
		} else if (creationStatus[2] == false) {
			System.out.println("Graph Created Without Bridge Edges, No hierarchy traversal possible");
			return true;
		} else if (creationStatus[3] == false) {
			System.out.println("Graph Created Without Causal Edges");
			return true;
		} else {
			// Everything true
			return true;
		}
	}
}
