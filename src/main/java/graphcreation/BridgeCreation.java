package graphcreation;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.NoSuchElementException;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;


import org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.GraphTraversalSource;
import org.apache.tinkerpop.gremlin.structure.Edge;
import org.janusgraph.core.JanusGraph;
import org.janusgraph.core.JanusGraphTransaction;
import org.janusgraph.core.JanusGraphVertex;

import utils.SplitStringToListUtil;

/*
	This module helps in creation of bridge edges of the graph  
	in watch tower.

	Attributes
	----------
	yaml_file : .yaml file
   Configuration file which contains all the 
   properties of a node , edge and all the other 
   data files and graph database location 

	causal_file : .txt 
 	Contains the data to load into the graph.
   
	Examples
	--------
	The addCausalEdge function will be called from 
	a separate wrapper as an object of BridgeCreation class	:
	
	obj = addBridgeEdge("causal_file","test.yaml");
*/

public class BridgeCreation {

	// private long BATCH_SIZE;
	// private String db_file;
	private JanusGraph graph;
	private long batchSize;
	private String dataFileName;
	private int numberOfBridgeEdges;

	public BridgeCreation(JanusGraph graph, long batchSize, String dataFileName) {
		this.graph = graph;
		this.batchSize = batchSize;
		this.dataFileName = dataFileName;
		this.numberOfBridgeEdges = 0;

	}

	private String calculateOverallContribution(String childValuesAsString, String parentValuesAsString) {

		// This flag is to initialise all the elements as 1 in the overall contribution
		// Post Ratio variable inclusion, this will be changed
		boolean onesOption = true;

		SplitStringToListUtil splitObj = new SplitStringToListUtil();
		Double[] childValues = splitObj.parseCommaSeparatedStringToListOfDouble(childValuesAsString);
		Double[] parentValues = splitObj.parseCommaSeparatedStringToListOfDouble(parentValuesAsString);

		Double[] overallContribution = new Double[childValues.length];
		for (int i = 0; i < overallContribution.length; i++) {
			if (onesOption == false) {
				if(parentValues[i] == 0){
					overallContribution[i] = 1d;
				}
				else{
				overallContribution[i] = childValues[i] / parentValues[i];
				}
			} else {
				overallContribution[i] = 1d;
			}
		}

		return Arrays.toString(overallContribution);

	}


	public int initiateBridgeCreation() {
        try{
            createBridgeEdges();
            return numberOfBridgeEdges;
        }

        catch (Exception e){
            System.out.print("\n\n\n####################  ERROR  ##########################\n");
            System.out.print(e.getMessage()+"\n");
            e.printStackTrace();
            System.out.print("\n##############################################\n\n\n");
            return 0;
        }
    }


	private void createBridgeEdges() {

		File f = new File(dataFileName);
		List<String> requiredPropertiesFromCSV = new ArrayList<String> ();
		requiredPropertiesFromCSV.add("key");
        requiredPropertiesFromCSV.add("kpi");
        requiredPropertiesFromCSV.add("parent_keys");
		try{
			LineIterator it = FileUtils.lineIterator(f, "UTF-8"); 
            int i = 0;
            String firstLine = it.nextLine();
			String[] columns = firstLine.split("\\|");
			JanusGraphTransaction tnx = graph.newTransaction();
			GraphTraversalSource g = tnx.traversal();
			String parent_keys;
			String key;
			String kpi;
			String current_id;
			String[] keys_list;
			String parent_id;
			JanusGraphVertex v1, v2;
			String overallContribution;
			Edge Bridge;
			HashMap record = new HashMap<String, String>();

			String currentLine;
            String currentColumn;
            String currentValue;
            while (it.hasNext()){
                currentLine = it.nextLine();
                String[] values = currentLine.split("\\|");
                for (int valuePos = 0; valuePos < values.length; valuePos++) { 
                    currentColumn = columns[valuePos];
                    if(requiredPropertiesFromCSV.contains(currentColumn)){
                        currentValue = values[valuePos];
                        record.put(currentColumn, currentValue);
                    }
                }
				parent_keys = record.get("parent_keys").toString();

				// Get the key and kpi values in order the construct the ID of the current node
				key = record.get("key").toString();
				kpi = record.get("kpi").toString();
				current_id = kpi + "-*-" + key;

				// If parents_keys is null then go to next record
				if (parent_keys == null) {
					continue;
				}

				// Else carry on with Bridge insertion

				// There can be multiple parents for Hybrid nodes, hence split the parent_keys
				// by ","
				keys_list = parent_keys.split(",");
				// Create bridge for each parent key post splitting
				for (int j = 0; j < keys_list.length; j++) {
					
					parent_id = keys_list[j];
					
					try{
						v1 = (JanusGraphVertex) g.V().has("ID", parent_id).next();
						v2 = (JanusGraphVertex) g.V().has("ID", current_id).next();
						overallContribution = calculateOverallContribution((String)g.V(v2).values("value").next(),(String)g.V(v1).values("value").next());
						Bridge = v2.addEdge("Bridge", v1);
						Bridge.property("betas", overallContribution);
						Bridge.property("lag", 0);
					}
					catch (NoSuchElementException e){
						// If either of the toKPI and fromKPI is not present in the graph
						// then we will ignore the current row and proceed
						continue;
					}
					catch (Exception e){
						e.printStackTrace();
						continue;
					}
					i++;

					// Logic to commit the transaction to the graph backend ones the Batch Size is
					// reached
					// Post commiting we will create a new transaction to proceef futher
					if (i % batchSize == 0) {
						try {
							g.close();
						} 
						catch (Exception e) {
							e.printStackTrace();
						}
						tnx.commit();
						tnx.close();
						tnx = graph.newTransaction();
						g = tnx.traversal();
						System.out.println("Bridge periodic commit at : " + i);
					}

				}
			}
		
		// If the number of nodes are not a multiple of batchSize 
        // then last few elements wouldn't have been commited and the transaction will open
        // Hence in such cases, things have to be committed and the transaction should be closed
		if (tnx.isOpen()) {
			try {
				g.close();
			} 
			catch (Exception e) {
				e.printStackTrace();
			}
			tnx.commit();
			tnx.close();
			
			System.out.println("Bridge Final commit at : " + i);
		}
		numberOfBridgeEdges = i;

		}catch(FileNotFoundException e){
            e.printStackTrace();
        } catch(IOException e){
            e.printStackTrace();
		}
		

	}

}
