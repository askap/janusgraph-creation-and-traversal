package graphcreation;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.NoSuchElementException;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;
import org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.GraphTraversalSource;
import org.apache.tinkerpop.gremlin.structure.Edge;
import org.janusgraph.core.JanusGraph;
import org.janusgraph.core.JanusGraphTransaction;
import org.janusgraph.core.JanusGraphVertex;


/*
  	This module helps in creation of causal edges of the graph  
	in watch tower.
   
   	Attributes
   	----------
   	yaml_file : .yaml file
       Configuration file which contains all the 
       properties of a node , edge and all the other 
       data files and graph database location 
    
    causal_file : .txt 
     	Contains the data to load into the graph.
       
   	Examples
   	--------
   	The addCausalEdge function will be called from 
    a separate wrapper as an object of CausalCreation class	:
   	
   	obj = addCausalEdge("causal_file","test.yaml");
*/




public class CausalCreation {
	
	// private long BATCH_SIZE;
	// private String db_file;
	private JanusGraph graph;
	private long batchSize;
	private String causalFileName;
	private int numberOfCausalEdges;

	public CausalCreation(JanusGraph graph, long batchSize, String causalFileName) {
		this.graph = graph;
		this.batchSize = batchSize;
		this.causalFileName = causalFileName;
		this.numberOfCausalEdges = 0;
	}

	public int initiateCausalCreation() {
        try{
			createCausalEdges();
            return numberOfCausalEdges;
        }

        catch (Exception e){
            System.out.print("\n\n\n####################  ERROR  ##########################\n");
            System.out.print(e.getMessage()+"\n");
            e.printStackTrace();
            System.out.print("\n##############################################\n\n\n");
            return 0;
        }
    }

	private void createCausalEdges() {

		File f = new File(causalFileName);
		List<String> requiredPropertiesFromCSV = new ArrayList<String> ();
		requiredPropertiesFromCSV.add("from_kpi");
        requiredPropertiesFromCSV.add("to_kpi");
		requiredPropertiesFromCSV.add("value");
		requiredPropertiesFromCSV.add("estimate");
        requiredPropertiesFromCSV.add("lag");
        requiredPropertiesFromCSV.add("hierarchy");
		try {
			LineIterator it = FileUtils.lineIterator(f, "UTF-8"); 
			int i = 0;
			String firstLine = it.nextLine();
			String[] columns = firstLine.split("\\|");
			JanusGraphTransaction tnx = graph.newTransaction();
			GraphTraversalSource g = tnx.traversal();

			String toKPI;
			String fromKPI;
			String idChunkValue;
			String betas;
			int lag;
			Edge Causal;
			String property;
			String informationFromfile;
			HashMap record = new HashMap<String, String>();
			String currentLine;
            String currentColumn;
            String currentValue;

			while (it.hasNext()) {
				currentLine = it.nextLine();
                String[] values = currentLine.split("\\|");
                for (int valuePos = 0; valuePos < values.length; valuePos++) { 
                    currentColumn = columns[valuePos];
                    if(requiredPropertiesFromCSV.contains(currentColumn)){
                        currentValue = values[valuePos];
                        record.put(currentColumn, currentValue);
                    }
				}
				
				toKPI = record.get("to_kpi").toString();
				fromKPI = record.get("from_kpi").toString();
				idChunkValue = record.get("value").toString();
				betas = record.get("estimate").toString();
				lag = Integer.parseInt(record.get("lag").toString());
				toKPI = toKPI + "-*-" + idChunkValue;
				fromKPI = fromKPI + "-*-" + idChunkValue;

				JanusGraphVertex v1;
				JanusGraphVertex v2;
				try {
					// Try block will be executed if the toKPI and fromKPi is present in the graph
					v1 = (JanusGraphVertex) g.V().has("ID", toKPI).next();
					v2 = (JanusGraphVertex) g.V().has("ID", fromKPI).next();
					Causal = v2.addEdge("Causal", v1);
					Causal.property("betas", "[" + betas + "]");
					Causal.property("lag",lag);
					Causal.property("hierarchy", record.get("hierarchy"));

				}
				catch (NoSuchElementException e){
					// If either of the toKPI and fromKPI is not present in the graph
					// then we will ignore the current row and proceed
					continue;
				}
				catch (Exception e){
					e.printStackTrace();
					continue;
				}
				// Increment i only if the insertion was successful
				i++;
				// Logic to commit the transaction to the graph backend ones the Batch Size is
				// reached
				// Post commiting we will create a new transaction to proceef futher
				if (i % batchSize == 0) {
					try {
						g.close();
					} 
					catch (Exception e) {
						e.printStackTrace();
					}
					tnx.commit();
					tnx.close();
					
					tnx = graph.newTransaction();
					g = tnx.traversal();
					System.out.println("Causal periodic commit at : " + i);
				}
				numberOfCausalEdges = i;


			}

			// If the number of nodes are not a multiple of batchSize 
			// then last few elements wouldn't have been commited and the transaction will open
			// Hence in such cases, things have to be committed and the transaction should be closed
			if (tnx.isOpen()) {
				try {
					g.close();
				} 
				catch (Exception e) {
					e.printStackTrace();
				}
				tnx.commit();
				tnx.close();
				
				System.out.println("Causal Final commit at : " + i);
			}
		}catch(IOException e){
            e.printStackTrace();
        }


	}

	
}
