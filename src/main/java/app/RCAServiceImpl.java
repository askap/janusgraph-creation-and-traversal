package app;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.janusgraph.core.JanusGraph;
import org.springframework.stereotype.Service;

import graphcreation.BridgeCreation;
import graphcreation.CausalCreation;
import graphcreation.GraphCreation;
import graphcreation.NodeCreation;
import graphcreation.SchemaCreation;
import traversal.RootCauseTraversalWrapper;
import traversal.TraversalForMultipleTimeMultipeLevel;

@Service
public class RCAServiceImpl implements RCAService {

	public boolean runRootCauseTraversal(RCATraversalDto rcaDto) {
		RootCauseTraversalWrapper obj = new RootCauseTraversalWrapper(rcaDto);
		boolean status = obj.runRootCauseTraversal();
		System.out.println(status);
		return status;
	}

	public boolean runRootCauseTraversalWithConcurrency(TraversalConcurrentDto traversalConcurrentDto) {
		TraversalForMultipleTimeMultipeLevel obj = new TraversalForMultipleTimeMultipeLevel(traversalConcurrentDto);
		boolean status = obj.initiateTraversal(true);
		System.out.println(status);
		return status;
	}

	public boolean runRootCauseTraversalWithoutConcurrency(TraversalConcurrentDto traversalConcurrentDto) {
		TraversalForMultipleTimeMultipeLevel obj = new TraversalForMultipleTimeMultipeLevel(traversalConcurrentDto);
		boolean status = obj.initiateTraversal(false);
		System.out.println(status);
		return status;
	}
}
