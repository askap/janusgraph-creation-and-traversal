package app;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.validation.constraints.NotNull;

import org.janusgraph.core.JanusGraph;

public class NodeCreationDto {
	
	@NotNull
	public JanusGraph graph;
	@NotNull
	public long batchSize;
	@NotNull
	public String dataFile;
	@NotNull
	public ArrayList<String> nodeKeyColumns;
	@NotNull
	public List<Map<String, Object>> nodeProperties;
	@NotNull
	public List<String> listPropertiesInNode;

	public JanusGraph getGraph() {
		return graph;
	}
	public void setGraph(JanusGraph graph) {
		this.graph = graph;
	}
	public long getBatchSize() {
		return batchSize;
	}
	public void setBatchSize(long batchSize) {
		this.batchSize = batchSize;
	}
	public NodeCreationDto() {
		super();
	}
	public String getDataFile() {
		return dataFile;
	}
	public void setDataFile(String dataFile) {
		this.dataFile = dataFile;
	}
	public NodeCreationDto(@NotNull JanusGraph graph, @NotNull long batchSize, @NotNull String dataFile,
			@NotNull ArrayList<String> nodeKeyColumns, @NotNull  List<Map<String, Object>> nodeProperties
			,@NotNull List<String> listPropertiesInNode) {
		super();
		this.graph = graph;
		this.batchSize = batchSize;
		this.dataFile = dataFile;
		this.nodeKeyColumns = nodeKeyColumns;
		this.nodeProperties = nodeProperties;
		this.listPropertiesInNode = listPropertiesInNode;
	}
	public ArrayList<String> getNodeKeyColumns() {
		return nodeKeyColumns;
	}
	public void setNodeKeyColumns(ArrayList<String> nodeKeyColumns) {
		this.nodeKeyColumns = nodeKeyColumns;
	}

	public List<Map<String, Object>> getNodeProperties(){
		return nodeProperties;
	}
	
	public void setNodeProperties(List<Map<String, Object>> nodeProperties){
		this.nodeProperties = nodeProperties;
	}

	public void setListPropertiesInNode(List<String> listPropertiesInNode){
		this.listPropertiesInNode = listPropertiesInNode;
	}

	public List<String> getListPropertiesInNode(){
		return listPropertiesInNode;
	}
	

}
