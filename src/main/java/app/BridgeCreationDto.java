package app;

import java.util.List;
import java.util.Map;

import javax.validation.constraints.NotNull;

import org.janusgraph.core.JanusGraph;

public class BridgeCreationDto {
	
	public BridgeCreationDto(@NotNull JanusGraph graph, @NotNull long batchSize, @NotNull String dataFile) {
		super();
		this.graph = graph;
		this.batchSize = batchSize;
		this.dataFile = dataFile;
	}

	@NotNull
	public JanusGraph graph;
	@NotNull
	public long batchSize;
	@NotNull
	public String dataFile;
	
	public JanusGraph getGraph() {
		return graph;
	}

	public void setGraph(JanusGraph graph) {
		this.graph = graph;
	}

	public long getBatchSize() {
		return batchSize;
	}

	public void setBatchSize(long batchSize) {
		this.batchSize = batchSize;
	}

	public String getDataFile() {
		return dataFile;
	}

	public void setDataFile(String dataFile) {
		this.dataFile = dataFile;
	}

	public BridgeCreationDto() {
		super();
	}
	

}
