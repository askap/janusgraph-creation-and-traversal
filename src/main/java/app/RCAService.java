package app;

public interface RCAService {
	boolean runRootCauseTraversal(RCATraversalDto rcaDto);
	boolean runRootCauseTraversalWithConcurrency(TraversalConcurrentDto traversalConcurrentDto);
	boolean runRootCauseTraversalWithoutConcurrency(TraversalConcurrentDto traversalConcurrentDto);
}