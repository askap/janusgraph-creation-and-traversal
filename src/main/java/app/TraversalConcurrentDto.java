package app;

import java.util.Arrays;
import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;

public class TraversalConcurrentDto {
	
	
	public TraversalConcurrentDto() {
		super();
	}
	public TraversalConcurrentDto(@NotNull List<String> levelsToTraverse, @NotNull List<String> datesToTraverse,
			@NotNull String time_column, @NotNull Integer root_cause_max_depth,
			@NotNull Integer impact_max_depth, @NotNull String root_cause_results_write_folder,
			@NotNull String impact_results_write_folder, @NotNull Integer range_for_anomaly,
			@NotNull String db_file, @Null Double filteringVariableThreshold, @NotNull List<String> requiredAnomalyAlgoFamilies
			,@Null List<String> requiredKPIs) {
		super();
		this.levelsToTraverse = levelsToTraverse;
		this.datesToTraverse = datesToTraverse;
		this.time_column = time_column;
		this.root_cause_max_depth = root_cause_max_depth;
		this.impact_max_depth = impact_max_depth;
		this.root_cause_results_write_folder = root_cause_results_write_folder;
		this.impact_results_write_folder = impact_results_write_folder;
		this.range_for_anomaly = range_for_anomaly;
		this.db_file = db_file;
		this.filteringVariableThreshold = null;
		if(filteringVariableThreshold != null){
			this.filteringVariableThreshold = filteringVariableThreshold;

		}
		
		this.requiredAnomalyAlgoFamilies = requiredAnomalyAlgoFamilies;
		
		this.requiredKPIs = null;
		if(requiredKPIs != null){
			this.requiredKPIs = requiredKPIs;
		}
		
		
	}
	@NotNull 
	private List<String> levelsToTraverse;
	@NotNull
	private List<String> datesToTraverse;
	@NotNull
	private String time_column;
	@NotNull
	private Integer root_cause_max_depth;
	@NotNull
	private Integer impact_max_depth;
	@NotNull
	private String root_cause_results_write_folder;
	@NotNull
	private String impact_results_write_folder;
	@NotNull
	private Integer range_for_anomaly;
	@NotNull
	private String db_file;
	@Null
	private Double filteringVariableThreshold;
	@NotNull
	private String anomaly_experiment_id;
	@NotNull
	private String anomaly_run_id;
	@NotNull
	private String causal_experiment_id;
	@NotNull
	private String causal_run_id;
	@NotNull
	private String data_source_id;
	@Null
	private String cid;
	@NotNull
	private List<String> requiredAnomalyAlgoFamilies;
	@Null
	private List<String> requiredKPIs;

	public List<String> getLevelsToTraverse() {
		return levelsToTraverse;
	}
	public void setLevelsToTraverse(List<String> levelsToTraverse) {
		this.levelsToTraverse = levelsToTraverse;
	}
	public List<String> getDatesToTraverse() {
		return datesToTraverse;
	}
	public void setDatesToTraverse(List<String> datesToTraverse) {
		this.datesToTraverse = datesToTraverse;
	}
	public String getTime_column() {
		return time_column;
	}
	public void setTime_column(String time_column) {
		this.time_column = time_column;
	}
	public Integer getRoot_cause_max_depth() {
		return root_cause_max_depth;
	}
	public void setRoot_cause_max_depth(Integer root_cause_max_depth) {
		this.root_cause_max_depth = root_cause_max_depth;
	}
	public Integer getImpact_max_depth() {
		return impact_max_depth;
	}
	public void setImpact_max_depth(Integer impact_max_depth) {
		this.impact_max_depth = impact_max_depth;
	}
	public String getRoot_cause_results_write_folder() {
		return root_cause_results_write_folder;
	}
	public void setRoot_cause_results_write_folder(String root_cause_results_write_folder) {
		this.root_cause_results_write_folder = root_cause_results_write_folder;
	}
	public String getImpact_results_write_folder() {
		return impact_results_write_folder;
	}
	public void setImpact_results_write_folder(String impact_results_write_folder) {
		this.impact_results_write_folder = impact_results_write_folder;
	}
	public Integer getRange_for_anomaly() {
		return range_for_anomaly;
	}
	public void setRange_for_anomaly(Integer range_for_anomaly) {
		this.range_for_anomaly = range_for_anomaly;
	}
	public String getDb_file() {
		return db_file;
	}
	public void setDb_file(String db_file) {
		this.db_file = db_file;
	}
	public Double getFilteringVariableThreshold() {
		return filteringVariableThreshold;
	}
	public void setFilteringVariableThreshold(Double filteringVariableThreshold) {
		this.filteringVariableThreshold = filteringVariableThreshold;
	}
	public String getAnomaly_experiment_id() {
		return anomaly_experiment_id;
	}
	public void setAnomaly_experiment_id(String anomaly_experiment_id) {
		this.anomaly_experiment_id = anomaly_experiment_id;
	}
	public String getAnomaly_run_id() {
		return anomaly_run_id;
	}
	public void setAnomaly_run_id(String anomaly_run_id) {
		this.anomaly_run_id = anomaly_run_id;
	}
	public String getCausal_experiment_id() {
		return causal_experiment_id;
	}
	public void setCausal_experiment_id(String causal_experiment_id) {
		this.causal_experiment_id = causal_experiment_id;
	}
	public String getCausal_run_id() {
		return causal_run_id;
	}
	public void setCausal_run_id(String causal_run_id) {
		this.causal_run_id = causal_run_id;
	}
	public String getData_source_id() {
		return data_source_id;
	}
	public void setData_source_id(String data_source_id) {
		this.data_source_id = data_source_id;
	}
	public String getCid() {
		return cid;
	}
	public void setCid(String cid) {
		this.cid = cid;
	}
	public void setRequiredAnomalyAlgoFamilies(List<String> requiredAnomalyAlgoFamilies) {
		this.requiredAnomalyAlgoFamilies = requiredAnomalyAlgoFamilies;
	}
	public List<String> getRequiredAnomalyAlgoFamilies() {
		return requiredAnomalyAlgoFamilies;
	}
	public void setRequiredKPIs(List<String> requiredKPIs) {
		this.requiredKPIs = requiredKPIs;
	}
	public List<String> getRequiredKPIs() {
		return requiredKPIs;
	}
	
	/*
	public TraversalConcurrentDto(@NotNull List<String> levelsToTraverse, @NotNull List<String> datesToTraverse,
			@NotNull String time_column, @NotNull Integer root_cause_max_depth, @NotNull Integer impact_max_depth,
			@NotNull String root_cause_results_write_folder, @NotNull String impact_results_write_folder,
			@NotNull Integer range_for_anomaly, @NotNull String db_file, @Null Double filteringVariableThreshold,
			@NotNull String anomaly_experiment_id, @NotNull String anomaly_run_id, @NotNull String causal_experiment_id,
			@NotNull String causal_run_id, @NotNull String data_source_id, @Null String cid) {
		super();
		this.levelsToTraverse = levelsToTraverse;
		this.datesToTraverse = datesToTraverse;
		this.time_column = time_column;
		this.root_cause_max_depth = root_cause_max_depth;
		this.impact_max_depth = impact_max_depth;
		this.root_cause_results_write_folder = root_cause_results_write_folder;
		this.impact_results_write_folder = impact_results_write_folder;
		this.range_for_anomaly = range_for_anomaly;
		this.db_file = db_file;
		this.filteringVariableThreshold = filteringVariableThreshold;
		if(filteringVariableThreshold == 0){
			this.filteringVariableThreshold = null;
		}
		this.anomaly_experiment_id = anomaly_experiment_id;
		this.anomaly_run_id = anomaly_run_id;
		this.causal_experiment_id = causal_experiment_id;
		this.causal_run_id = causal_run_id;
		this.data_source_id = data_source_id;
		this.cid = cid;
	}
	*/
	@Override
	public String toString() {
		return "TraversalConcurrentDto [levelsToTraverse=" + levelsToTraverse + ", datesToTraverse=" + datesToTraverse
				+ ", time_column=" + time_column + ", root_cause_max_depth=" + root_cause_max_depth
				+ ", impact_max_depth=" + impact_max_depth + ", root_cause_results_write_folder="
				+ root_cause_results_write_folder + ", impact_results_write_folder=" + impact_results_write_folder
				+ ", range_for_anomaly=" + range_for_anomaly + ", db_file=" + db_file + ", filteringVariableThreshold="
				+ filteringVariableThreshold + ", anomaly_experiment_id=" + anomaly_experiment_id + ", anomaly_run_id="
				+ anomaly_run_id + ", causal_experiment_id=" + causal_experiment_id + ", causal_run_id=" + causal_run_id
				+ ", data_source_id=" + data_source_id 
				+ ", cid=" + cid +", requiredAnomalyAlgoFamilies=" + requiredAnomalyAlgoFamilies + "]";
	}

	
	
	
}
