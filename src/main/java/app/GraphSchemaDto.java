package app;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.validation.constraints.NotNull;

import org.janusgraph.core.JanusGraph;

public class GraphSchemaDto {
	
	@NotNull
	public JanusGraph graph;
	@NotNull
	public List<Map<String, Object>> bridgeProperties;
	@NotNull
	public List<Map<String, Object>> nodeProperties;
	@NotNull
	public List<Map<String, Object>> causalProperties;
	@NotNull
	public ArrayList<String> nodeCompositeIndexProperties;
	public ArrayList<String> getNodeCompositeIndexProperties() {
		return nodeCompositeIndexProperties;
	}

	public void setNodeCompositeIndexProperties(ArrayList<String> nodeCompositeIndexProperties) {
		this.nodeCompositeIndexProperties = nodeCompositeIndexProperties;
	}

	public List<Map<String, Object>> getNodeProperties() {
		return nodeProperties;
	}
	
	public void setNodeProperties(List<Map<String, Object>> nodeProperties) {
		this.nodeProperties = nodeProperties;
	}
	public GraphSchemaDto() {
		super();
	}
	
	public GraphSchemaDto(@NotNull JanusGraph graph, @NotNull List<Map<String, Object>> bridgeProperties,
			@NotNull List<Map<String, Object>> nodeProperties, @NotNull List<Map<String, Object>> causalProperties,
			@NotNull ArrayList<String> nodeCompositeIndexProperties) {
		super();
		this.graph = graph;
		this.bridgeProperties = bridgeProperties;
		this.nodeProperties = nodeProperties;
		this.causalProperties = causalProperties;
		this.nodeCompositeIndexProperties = nodeCompositeIndexProperties;
	}

	public JanusGraph getGraph() {
		return graph;
	}
	public void setGraph(JanusGraph graph) {
		this.graph = graph;
	}
	public List<Map<String, Object>> getBridgeProperties() {
		return bridgeProperties;
	}
	public void setBridgeProperties(List<Map<String, Object>> bridgeProperties) {
		this.bridgeProperties = bridgeProperties;
	}
	public List<Map<String, Object>> getCausalProperties() {
		return causalProperties;
	}
	public void setCausalProperties(List<Map<String, Object>> causalProperties) {
		this.causalProperties = causalProperties;
	}

	

}
