package app;

import javax.validation.constraints.NotNull;

public class GraphCreationDto {
	
	@NotNull
	private String data_file;
	@NotNull
	private String causal_file;
	@NotNull
	private String db_property_file;
	
	
	public GraphCreationDto() {
		super();
	}

	public GraphCreationDto(@NotNull String data_file, @NotNull String causal_file, @NotNull String db_property_file) {
		super();
		this.data_file = data_file;
		this.causal_file = causal_file;
		this.db_property_file = db_property_file;
	}

	public String getData_file() {
		return data_file;
	}

	public void setData_file(String data_file) {
		this.data_file = data_file;
	}

	public String getCausal_file() {
		return causal_file;
	}

	public void setCausal_file(String causal_file) {
		this.causal_file = causal_file;
	}

	public String getDb_property_file() {
		return db_property_file;
	}

	public void setDb_property_file(String db_property_file) {
		this.db_property_file = db_property_file;
	}

	@Override
	public String toString() {
		return "GraphCreationDto [data_file=" + data_file + ", causal_file=" + causal_file + ", db_property_file="
				+ db_property_file + "]";
	}

}
