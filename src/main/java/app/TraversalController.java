/**
 * 
 */
package app;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import javax.ws.rs.QueryParam;

import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author vishnu
 *
 */
@RestController
@RequestMapping("/traversal")
public class TraversalController {
	private RCAService rcaService;
	private String cid;

	@Autowired
	public TraversalController(RCAService rcaService) {
		this.rcaService = rcaService;
	}

	public void stopDocker() {

		CloseableHttpClient httpClient = HttpClients.createDefault();
		HttpPost httppost = new HttpPost("http://localhost:5000/api/anomaly/update_docker/" + cid);

		// Request parameters and other properties.
		List<NameValuePair> params = new ArrayList<NameValuePair>(2);
		params.add(new BasicNameValuePair("cid", cid));

		try {

			httppost.setEntity(new UrlEncodedFormEntity(params));
			CloseableHttpResponse response = httpClient.execute(httppost);

		} catch (UnsupportedEncodingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public boolean runWithConcurrency(TraversalConcurrentDto traversalConcurrentDto) {

		long startTime = System.currentTimeMillis();
		boolean status;
		status = rcaService.runRootCauseTraversalWithConcurrency(traversalConcurrentDto);
		System.out.println("End Status: " + status);
		System.out.println("\n\nTotal Time taken for Parallel Execution: "
				+ (System.currentTimeMillis() - startTime) / 1000 + " seconds");
		// For Stopping Docker
		cid = traversalConcurrentDto.getCid();
		if(cid != null){
			stopDocker();
		}
		return status;
	}

	public boolean runWithoutConcurrency(TraversalConcurrentDto traversalConcurrentDto) {

		long startTime = System.currentTimeMillis();
		boolean status;
		status = rcaService.runRootCauseTraversalWithoutConcurrency(traversalConcurrentDto);
		System.out.println("End Status: " + status);
		System.out.println("\n\nTotal Time taken for Linear Execution: "
				+ (System.currentTimeMillis() - startTime) / 1000 + " seconds");
		// For Stopping Docker
		cid = traversalConcurrentDto.getCid();
		if(cid != null){
			stopDocker();
		}
		return status;

	}

	@PostMapping(path = "/create/{enable_concurrency}")
	public ResponseEntity<?> createRCAAndImpact(@RequestBody TraversalConcurrentDto traversalConcurrentDto,
			@QueryParam("enable_concurrency") boolean enable_concurrency) throws Exception {
		if (enable_concurrency) {
			CompletableFuture<Boolean> completableFuture = CompletableFuture
					.supplyAsync(() -> runWithConcurrency(traversalConcurrentDto));
		} else {
			CompletableFuture<Boolean> completableFuture = CompletableFuture
					.supplyAsync(() -> runWithoutConcurrency(traversalConcurrentDto));
		}
		return new ResponseEntity<>(HttpStatus.CREATED);
	}

}
