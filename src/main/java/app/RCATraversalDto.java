package app;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;

public class RCATraversalDto {
	

	@NotNull
    private String config_file;
	@NotNull
	private String the_date;
	@NotNull
	private String time_column;
	@NotNull
	private Integer max_depth;
	@NotNull
	private String level_name;
	@NotNull
	private String write_folder;
	@NotNull
	private Integer range_for_anomaly;
	@Null
	private Double filteringVariableThreshold;
	
	public String getConfig_file() {
		return config_file;
	}
	public void setConfig_file(String config_file) {
		this.config_file = config_file;
	}
	public String getThe_date() {
		return the_date;
	}
	public void setThe_date(String the_date) {
		this.the_date = the_date;
	}
	public String getTime_column() {
		return time_column;
	}
	public void setTime_column(String time_column) {
		this.time_column = time_column;
	}
	public Integer getMax_depth() {
		return max_depth;
	}
	public void setMax_depth(Integer max_depth) {
		this.max_depth = max_depth;
	}
	public String getLevel_name() {
		return level_name;
	}
	public void setLevel_name(String level_name) {
		this.level_name = level_name;
	}
	public String getWrite_folder() {
		return write_folder;
	}
	public void setWrite_folder(String write_folder) {
		this.write_folder = write_folder;
	}
	public Integer getRange_for_anomaly() {
		return range_for_anomaly;
	}
	public void setRange_for_anomaly(Integer range_for_anomaly) {
		this.range_for_anomaly = range_for_anomaly;
	}

	// Filtering Variable threshold
	public Double getFilteringVariableThreshold() {
		return filteringVariableThreshold;
	}
	public void setFilteringVariableThreshold(Double filteringVariableThreshold) {
		this.filteringVariableThreshold = filteringVariableThreshold;
	}
	
	
	public RCATraversalDto(@NotNull String config_file, @NotNull String the_date, @NotNull String time_column,
			@NotNull Integer max_depth, @NotNull String level_name, @NotNull String write_folder,
			@NotNull Integer range_for_anomaly, @Null Double filteringVariableThreshold) {
		super();
		this.config_file = config_file;
		this.the_date = the_date;
		this.time_column = time_column;
		this.max_depth = max_depth;
		this.level_name = level_name;
		this.write_folder = write_folder;
		this.range_for_anomaly = range_for_anomaly;
		this.filteringVariableThreshold = null;
		if(filteringVariableThreshold != null){
			this.filteringVariableThreshold = filteringVariableThreshold;

		}
	}
	
}
