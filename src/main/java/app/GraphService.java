package app;

public interface GraphService {
	boolean initiateCreateGraph(GraphCreationDto graphConfig);

	void createInferences(WTPipelineDto pipelineConfig);

	boolean createSchema(GraphSchemaDto graphSchemaDto);

	int createGraphNodes(NodeCreationDto nodeCreationDto);

	int createGraphBridges(BridgeCreationDto bridgeCreationDto);

	int createGraphCausals(CausalCreationDto causalCreationDto);
	
	boolean runRootCauseTraversal(RCATraversalDto rcaDto);
	
	boolean runRootCauseTraversalWithConcurrency(TraversalConcurrentDto traversalConcurrentDto);
	
	boolean runRootCauseTraversalWithoutConcurrency(TraversalConcurrentDto traversalConcurrentDto);
}