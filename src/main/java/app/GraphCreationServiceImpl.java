package app;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.janusgraph.core.JanusGraph;
import org.springframework.stereotype.Service;

import graphcreation.BridgeCreation;
import graphcreation.CausalCreation;
import graphcreation.GraphCreation;
import graphcreation.NodeCreation;
import graphcreation.SchemaCreation;
import traversal.RootCauseTraversalWrapper;
import traversal.TraversalForMultipleTimeMultipeLevel;

@Service
public class GraphCreationServiceImpl implements GraphService {

	public boolean initiateCreateGraph(GraphCreationDto graphConfig) {

		GraphCreation graphObj = new GraphCreation(graphConfig);
		boolean statusCode = graphObj.initiateCreateGraph();
		System.out.println(statusCode);
		return statusCode;
	}

	public boolean createSchema(GraphSchemaDto graphSchemaDto) {
		JanusGraph graph = graphSchemaDto.getGraph();
		List<Map<String, Object>> bridgeProperties = graphSchemaDto.getBridgeProperties();
		List<Map<String, Object>> causalProperties = graphSchemaDto.getCausalProperties();
		List<Map<String, Object>> nodeProperties = graphSchemaDto.getNodeProperties();
		ArrayList<String> nodeCompositeIndexProperties = graphSchemaDto.getNodeCompositeIndexProperties();

		SchemaCreation schemaObj = new SchemaCreation(graph, nodeProperties, bridgeProperties, causalProperties,
				nodeCompositeIndexProperties);
		boolean processStatus;
		processStatus = schemaObj.createSchema();

		System.out.println(processStatus);
		return processStatus;
	}

	public int createGraphNodes(NodeCreationDto nodeCreationDto) {
		JanusGraph graph = nodeCreationDto.getGraph();
		long batchSize = nodeCreationDto.getBatchSize();
		String dataFile = nodeCreationDto.getDataFile();
		ArrayList<String> nodeKeyColumns = nodeCreationDto.getNodeKeyColumns();
		List<Map<String, Object>> nodeProperties = nodeCreationDto.getNodeProperties();
		List<String> listPropertiesInNode = nodeCreationDto.getListPropertiesInNode();
		NodeCreation nodeObj = new NodeCreation(graph, batchSize, dataFile, nodeKeyColumns, nodeProperties, listPropertiesInNode);
		int processStatus;
		processStatus = nodeObj.initiateNodeCreation();
		graph.tx().rollback();
		System.out.println(processStatus);
		return processStatus;
	}

	public int createGraphBridges(BridgeCreationDto bridgeCreationDto) {
		JanusGraph graph = bridgeCreationDto.getGraph();
		long batchSize = bridgeCreationDto.getBatchSize();
		String dataFile = bridgeCreationDto.getDataFile();
		BridgeCreation bridgeObj = new BridgeCreation(graph, batchSize, dataFile);
		int processStatus;
		processStatus = bridgeObj.initiateBridgeCreation();

		graph.tx().rollback();

		System.out.println(processStatus);
		return processStatus;
	}

	public int createGraphCausals(CausalCreationDto causalCreationDto) {

		JanusGraph graph = causalCreationDto.getGraph();
		long batchSize = causalCreationDto.getBatchSize();
		String causalFile = causalCreationDto.getCausalFile();
		CausalCreation causalObj = new CausalCreation(graph, batchSize, causalFile);
		int processStatus;
		processStatus = causalObj.initiateCausalCreation();

		graph.tx().rollback();
		System.out.println(processStatus);
		return processStatus;
	}

	public void createInferences(WTPipelineDto pipelineDto) {
		GraphCreationDto graphConfig = new GraphCreationDto();
		String data_file = pipelineDto.getData_file();
		String causal_file = pipelineDto.getCausal_file();
		String db_property_file = pipelineDto.getDb_property_file();
		graphConfig.setData_file(data_file);
		graphConfig.setCausal_file(causal_file);
		graphConfig.setDb_property_file(db_property_file);

		GraphCreation graphObj = new GraphCreation(graphConfig);

		boolean statusCode = graphObj.initiateCreateGraph();
		System.out.println(statusCode);
		if (statusCode) {
			TraversalConcurrentDto traversalConcurrentDto = new TraversalConcurrentDto();
			traversalConcurrentDto.setAnomaly_experiment_id(pipelineDto.getAnomaly_experiment_id());
			traversalConcurrentDto.setAnomaly_run_id(pipelineDto.getAnomaly_run_id());
			traversalConcurrentDto.setCausal_experiment_id(pipelineDto.getCausal_experiment_id());
			traversalConcurrentDto.setCausal_run_id(pipelineDto.getCausal_run_id());
			traversalConcurrentDto.setData_source_id(pipelineDto.getData_source_id());
			traversalConcurrentDto.setFilteringVariableThreshold(pipelineDto.getFilteringVariableThreshold());
			traversalConcurrentDto.setDatesToTraverse(pipelineDto.getDatesToTraverse());
			traversalConcurrentDto.setDb_file(pipelineDto.getDb_property_file());
			traversalConcurrentDto.setRoot_cause_results_write_folder(pipelineDto.getRoot_cause_results_write_folder());
			traversalConcurrentDto.setImpact_max_depth(pipelineDto.getImpact_max_depth());
			traversalConcurrentDto.setImpact_results_write_folder(pipelineDto.getImpact_results_write_folder());
			traversalConcurrentDto.setRoot_cause_max_depth(pipelineDto.getRoot_cause_max_depth());
			traversalConcurrentDto.setTime_column(pipelineDto.getTime_column());
			traversalConcurrentDto.setRange_for_anomaly(pipelineDto.getRange_for_anomaly());
			traversalConcurrentDto.setLevelsToTraverse(pipelineDto.getLevelsToTraverse());
			traversalConcurrentDto.setRequiredAnomalyAlgoFamilies(pipelineDto.getRequiredAnomalyAlgoFamilies());
			System.out.println("In GraphCreationServiceImpl\n"+traversalConcurrentDto.toString());
			RCAServiceImpl rcaServiceImpl = new RCAServiceImpl();
			rcaServiceImpl.runRootCauseTraversalWithoutConcurrency(traversalConcurrentDto);
		}
		System.out.println("FINISHED TRAVERSAL");

	}
	public boolean runRootCauseTraversal(RCATraversalDto rcaDto) {
		RootCauseTraversalWrapper obj = new RootCauseTraversalWrapper(rcaDto);
		boolean status = obj.runRootCauseTraversal();
		System.out.println(status);
		return status;
	}

	public boolean runRootCauseTraversalWithConcurrency(TraversalConcurrentDto traversalConcurrentDto) {
		TraversalForMultipleTimeMultipeLevel obj = new TraversalForMultipleTimeMultipeLevel(traversalConcurrentDto);
		boolean status = obj.initiateTraversal(true);
		System.out.println(status);
		return status;
	}

	public boolean runRootCauseTraversalWithoutConcurrency(TraversalConcurrentDto traversalConcurrentDto) {
		TraversalForMultipleTimeMultipeLevel obj = new TraversalForMultipleTimeMultipeLevel(traversalConcurrentDto);
		boolean status = obj.initiateTraversal(false);
		System.out.println(status);
		return status;
	}

}
