/**
 * 
 */
package app;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import graphcreation.GraphCreation;

/**
 * @author vishnu
 *
 */
@RestController
@RequestMapping("/graph")
public class GraphController {
	private GraphService graphService;
	private RCAService rcaService;
	private String cid;

	@Autowired
	public GraphController(GraphService graphService) {
		this.graphService = graphService;
	}

	@PostMapping(path = "/create_graph")
	public ResponseEntity<?> createGraph(@RequestBody GraphCreationDto graphConfig) throws Exception {
		System.out.println(graphConfig.toString());
		boolean statusCode = graphService.initiateCreateGraph(graphConfig);
		return new ResponseEntity<>(HttpStatus.CREATED);
	}

	public boolean runWithConcurrency(TraversalConcurrentDto traversalConcurrentDto) {

		long startTime = System.currentTimeMillis();
		boolean status;
		status = rcaService.runRootCauseTraversalWithConcurrency(traversalConcurrentDto);
		System.out.println("End Status: " + status);
		System.out.println("\n\nTotal Time taken for Parallel Execution: "
				+ (System.currentTimeMillis() - startTime) / 1000 + " seconds");
		return status;

	}

	public void stopDocker() {

		CloseableHttpClient httpClient = HttpClients.createDefault();
		HttpPost httppost = new HttpPost("http://localhost:5008/api/graph/update_docker/" + cid);

		// Request parameters and other properties.
		List<NameValuePair> params = new ArrayList<NameValuePair>(2);
		params.add(new BasicNameValuePair("cid", cid));

		try {

			httppost.setEntity(new UrlEncodedFormEntity(params));
			CloseableHttpResponse response = httpClient.execute(httppost);

		} catch (UnsupportedEncodingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public boolean runWithoutConcurrency(TraversalConcurrentDto traversalConcurrentDto) {

		long startTime = System.currentTimeMillis();
		boolean status;
		status = graphService.runRootCauseTraversalWithoutConcurrency(traversalConcurrentDto);
		System.out.println("End Status: " + status);
		System.out.println("\n\nTotal Time taken for Linear Execution: "
				+ (System.currentTimeMillis() - startTime) / 1000 + " seconds");
		cid = traversalConcurrentDto.getCid();
		if (cid != null) {
			stopDocker();
		}
		return status;

	}

	public boolean runRCA(WTPipelineDto pipelineDto) {

		long startTime = System.currentTimeMillis();
		GraphCreationDto graphConfig = new GraphCreationDto();
		System.out.println(pipelineDto.toString());
		String data_file = pipelineDto.getData_file();
		String causal_file = pipelineDto.getCausal_file();
		String db_property_file = pipelineDto.getDb_property_file();
		graphConfig.setData_file(data_file);
		graphConfig.setCausal_file(causal_file);
		graphConfig.setDb_property_file(db_property_file);
		boolean statusCode = graphService.initiateCreateGraph(graphConfig);
		boolean status = false;
		if (statusCode) {
			TraversalConcurrentDto traversalConcurrentDto = new TraversalConcurrentDto();
			traversalConcurrentDto.setAnomaly_experiment_id(pipelineDto.getAnomaly_experiment_id());
			traversalConcurrentDto.setAnomaly_run_id(pipelineDto.getAnomaly_run_id());
			traversalConcurrentDto.setCausal_experiment_id(pipelineDto.getCausal_experiment_id());
			traversalConcurrentDto.setCausal_run_id(pipelineDto.getCausal_run_id());
			traversalConcurrentDto.setData_source_id(pipelineDto.getData_source_id());
			traversalConcurrentDto.setFilteringVariableThreshold(pipelineDto.getFilteringVariableThreshold());
			traversalConcurrentDto.setDatesToTraverse(pipelineDto.getDatesToTraverse());
			traversalConcurrentDto.setDb_file(pipelineDto.getDb_property_file());
			traversalConcurrentDto.setRoot_cause_results_write_folder(pipelineDto.getRoot_cause_results_write_folder());
			traversalConcurrentDto.setImpact_max_depth(pipelineDto.getImpact_max_depth());
			traversalConcurrentDto.setImpact_results_write_folder(pipelineDto.getImpact_results_write_folder());
			traversalConcurrentDto.setRoot_cause_max_depth(pipelineDto.getRoot_cause_max_depth());
			traversalConcurrentDto.setTime_column(pipelineDto.getTime_column());
			traversalConcurrentDto.setRange_for_anomaly(pipelineDto.getRange_for_anomaly());
			traversalConcurrentDto.setLevelsToTraverse(pipelineDto.getLevelsToTraverse());
			traversalConcurrentDto.setCid(pipelineDto.getCid());
			traversalConcurrentDto.setRequiredAnomalyAlgoFamilies(pipelineDto.getRequiredAnomalyAlgoFamilies());
			traversalConcurrentDto.setRequiredKPIs(pipelineDto.getRequiredKPIs());
			System.out.println("In GraphController\n" + traversalConcurrentDto.toString());

			status = runWithoutConcurrency(traversalConcurrentDto);
		}
		return status;
	}

	@PostMapping(path = "/generate_inference")
	public ResponseEntity<?> createInferences(@RequestBody WTPipelineDto pipelineDto) throws Exception {
		CompletableFuture<Boolean> completableFuture = CompletableFuture.supplyAsync(() -> runRCA(pipelineDto));
		return new ResponseEntity<>(HttpStatus.CREATED);
	}
}
