package app;

public interface ImpactService {
	boolean runImpactAnalysis(ImpactTraversalDto impactDto);
	
}