package app;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.janusgraph.core.JanusGraph;
import org.springframework.stereotype.Service;

import graphcreation.BridgeCreation;
import graphcreation.CausalCreation;
import graphcreation.GraphCreation;
import graphcreation.NodeCreation;
import graphcreation.SchemaCreation;
import traversal.ImpactTraversalWrapper;
import traversal.RootCauseTraversalWrapper;

@Service
public class ImpactServiceImpl implements ImpactService{
	
	public boolean runImpactAnalysis(ImpactTraversalDto impactDto) {
		String jsonParams = "{\"config_file\" : \"test1.yaml\",\"the_date\" : \"2018-07-08\",\"time_column\" : \"time\",\"level_name\" : \"overall\",\"max_depth\" : 3,\"write_folder\" : \"RootCauseTraversalTrial_mvn_test\",\"range_for_anomaly\":2}";
		ImpactTraversalWrapper obj = new ImpactTraversalWrapper(jsonParams);
        boolean status = obj.runImpactAnalysis();
		System.out.println(status); 
		return status;
    }
}
