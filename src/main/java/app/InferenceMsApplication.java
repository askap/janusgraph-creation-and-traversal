package app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InferenceMsApplication {

	public static void main(String[] args) {
		SpringApplication.run(InferenceMsApplication.class, args);
	}

}
