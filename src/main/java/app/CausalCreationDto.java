package app;

import javax.validation.constraints.NotNull;

import org.janusgraph.core.JanusGraph;

public class CausalCreationDto {
	
	@NotNull
	public JanusGraph graph;
	@NotNull
	public long batchSize;
	@NotNull
	public String causalFile;
	public CausalCreationDto() {
		super();
	}
	public CausalCreationDto(@NotNull JanusGraph graph, @NotNull long batchSize, @NotNull String causalFile) {
		super();
		this.graph = graph;
		this.batchSize = batchSize;
		this.causalFile = causalFile;
	}
	public JanusGraph getGraph() {
		return graph;
	}
	public void setGraph(JanusGraph graph) {
		this.graph = graph;
	}
	public long getBatchSize() {
		return batchSize;
	}
	public void setBatchSize(long batchSize) {
		this.batchSize = batchSize;
	}
	public String getCausalFile() {
		return causalFile;
	}
	public void setCausalFile(String causalFile) {
		this.causalFile = causalFile;
	}

}
