/**
 * 
 */
package app;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import traversal.ImpactTraversalWrapper;
import traversal.RootCauseTraversalWrapper;

/**
 * @author vishnu
 *
 */
//@RestController
//@RequestMapping("/impact")
public class ImpactController {
	private ImpactService impactService;
	
	@Autowired
    public ImpactController(ImpactService impactService) {
        this.impactService = impactService;
    }
	@PostMapping(path="/create")
    public ResponseEntity<?> createImpact(@RequestBody ImpactTraversalDto impactDto) throws Exception {
        boolean status = impactService.runImpactAnalysis(impactDto);
        System.out.println(status);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

}
