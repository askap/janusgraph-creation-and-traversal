package app;

public interface SchemaCreationService {
	boolean createGraphSchema(GraphSchemaDto graphSchemaDto);
}