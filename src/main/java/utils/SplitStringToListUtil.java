package utils;

import com.google.common.base.CharMatcher;
import com.google.common.base.Splitter;
import com.google.common.collect.FluentIterable;
import com.google.common.primitives.Ints;
import com.google.common.primitives.Doubles;

public class SplitStringToListUtil {

    public Integer[] parseCommaSeparatedStringToListOfInteger(String s) {
        Iterable<String> i = Splitter.on(",").trimResults(CharMatcher.WHITESPACE.or(CharMatcher.anyOf("[]"))).split(s);
        Integer[] result = FluentIterable.from(i).transform(Ints.stringConverter()).toArray(Integer.class);
        return result;
    }


    public Iterable<String> parseCommaSeparateparseStringToListOfString(String s) {
        Iterable<String> stringIterable = Splitter.on(",").trimResults(CharMatcher.WHITESPACE.or(CharMatcher.anyOf("[]"))).split(s);
        return stringIterable;
    }

    public Double[] parseCommaSeparatedStringToListOfDouble(String s) {
        Iterable<String> i = Splitter.on(",").trimResults(CharMatcher.WHITESPACE.or(CharMatcher.anyOf("[]"))).split(s);
        Double[] result = FluentIterable.from(i).transform(Doubles.stringConverter()).toArray(Double.class);
        return result;
    }


    

}