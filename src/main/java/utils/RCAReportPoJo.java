package utils;

public class RCAReportPoJo {
    String anomaly_project_id;
    String anomaly_experiment_id;
    String causal_project_id;
    String causal_experiment_id;
    String level_to_consider;
    String date_to_run;
    String levelName;
    String theDate;

    public RCAReportPoJo() {
    }

    public RCAReportPoJo(String anomaly_project_id, String anomaly_experiment_id, String causal_project_id, String causal_experiment_id, String level_to_consider, String date_to_run, String levelName, String theDate) {
        this.anomaly_project_id = anomaly_project_id;
        this.anomaly_experiment_id = anomaly_experiment_id;
        this.causal_project_id = causal_project_id;
        this.causal_experiment_id = causal_experiment_id;
        this.level_to_consider = level_to_consider;
        this.date_to_run = date_to_run;
        this.levelName = levelName;
        this.theDate = theDate;
    }

    public String getAnomaly_project_id() {
        return this.anomaly_project_id;
    }

    public void setAnomaly_project_id(String anomaly_project_id) {
        this.anomaly_project_id = anomaly_project_id;
    }

    public String getAnomaly_experiment_id() {
        return this.anomaly_experiment_id;
    }

    public void setAnomaly_experiment_id(String anomaly_experiment_id) {
        this.anomaly_experiment_id = anomaly_experiment_id;
    }

    public String getCausal_project_id() {
        return this.causal_project_id;
    }

    public void setCausal_project_id(String causal_project_id) {
        this.causal_project_id = causal_project_id;
    }

    public String getCausal_experiment_id() {
        return this.causal_experiment_id;
    }

    public void setCausal_experiment_id(String causal_experiment_id) {
        this.causal_experiment_id = causal_experiment_id;
    }

    public String getLevel_to_consider() {
        return this.level_to_consider;
    }

    public void setLevel_to_consider(String level_to_consider) {
        this.level_to_consider = level_to_consider;
    }

    public String getDate_to_run() {
        return this.date_to_run;
    }

    public void setDate_to_run(String date_to_run) {
        this.date_to_run = date_to_run;
    }

    public String getLevelName() {
        return this.levelName;
    }

    public void setLevelName(String levelName) {
        this.levelName = levelName;
    }

    public String getTheDate() {
        return this.theDate;
    }

    public void setTheDate(String theDate) {
        this.theDate = theDate;
    }

    public RCAReportPoJo anomaly_project_id(String anomaly_project_id) {
        this.anomaly_project_id = anomaly_project_id;
        return this;
    }

    public RCAReportPoJo anomaly_experiment_id(String anomaly_experiment_id) {
        this.anomaly_experiment_id = anomaly_experiment_id;
        return this;
    }

    public RCAReportPoJo causal_project_id(String causal_project_id) {
        this.causal_project_id = causal_project_id;
        return this;
    }

    public RCAReportPoJo causal_experiment_id(String causal_experiment_id) {
        this.causal_experiment_id = causal_experiment_id;
        return this;
    }

    public RCAReportPoJo level_to_consider(String level_to_consider) {
        this.level_to_consider = level_to_consider;
        return this;
    }

    public RCAReportPoJo date_to_run(String date_to_run) {
        this.date_to_run = date_to_run;
        return this;
    }

    public RCAReportPoJo levelName(String levelName) {
        this.levelName = levelName;
        return this;
    }

    public RCAReportPoJo theDate(String theDate) {
        this.theDate = theDate;
        return this;
    }


}