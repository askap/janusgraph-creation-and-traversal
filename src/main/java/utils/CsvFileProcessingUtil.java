package utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.univocity.parsers.common.processor.RowListProcessor;
import com.univocity.parsers.csv.*;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.io.Reader;

public class CsvFileProcessingUtil {

    private String fileNameToProcess;
    private RowListProcessor rowProcessor = new RowListProcessor();
    private List<String> headerList;
    private CsvParser parser;

    public CsvFileProcessingUtil(String fileNameToProcess, String delimiter){
        this.fileNameToProcess = fileNameToProcess;
        this.rowProcessor = new RowListProcessor();
        
        CsvParserSettings settings = new CsvParserSettings();
        settings.getFormat().setDelimiter(delimiter);
        settings.setProcessor(this.rowProcessor);
        settings.setHeaderExtractionEnabled(true);
        settings.setMaxCharsPerColumn(40960);
        // settings.setMaxColumns(Integer.MAX_VALUE);
        this.parser = new CsvParser(settings);
    }

    private Reader getReader(String relativePath) {

        Reader in = null;
        try {
            in = new InputStreamReader(new FileInputStream(relativePath));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return in;
    }

    private void identifyHeaderNamesFromCsvFile(){

        //Parsing the Given file at this entirety
        parser.parse(getReader(fileNameToProcess));
        
        //getting column names from a CSV file 
        String[] headers = rowProcessor.getHeaders();
        headerList = new ArrayList<String>(Arrays.asList(headers));
    }

    public List<String> getHeaderNamesFromCsvFile(){
        identifyHeaderNamesFromCsvFile();
        return headerList;
    }

    private void assignRecordIterator(){
        parser.beginParsing(getReader(fileNameToProcess));
    }

    public CsvParser getRecordIterator(){
        assignRecordIterator();
        return parser;
    }

}