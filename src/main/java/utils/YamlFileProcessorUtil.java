package utils;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.yaml.snakeyaml.Yaml;

public class YamlFileProcessorUtil {

    private Map yamlMap;
    private ArrayList<String> nodeKeyColumns;
    private ArrayList<String> nodeCompositeIndexProperties;
    private List<Map<String, Object>> nodeProperties;
    private List<Map<String, Object>> bridgeProperties;
    private List<Map<String, Object>> causalProperties;
    private String databasePropertiesFile;
    private String dataFile;
    private String causalFile;
    private long batchSize;
    private ArrayList<String> historicalAnomalyFamlies;
    private ArrayList<String> multiAnomalyAlgoFamilies;
    private ArrayList<String> singleAnomalyAlgoFamilies;
    private ArrayList<String> listPropertiesInNode;
    private Map<String, List<String>> ruleEngineForRootCauses;

    public YamlFileProcessorUtil(String settingsFile) {
        InputStream stream = null;
        // stream = new FileInputStream(settingsFile);
        stream = getClass().getResourceAsStream(settingsFile);

        Yaml yaml = new Yaml();
        this.yamlMap = (Map) yaml.load(stream);

        try {
            // Assigning nodeKeyColumns
            nodeKeyColumns = new ArrayList<String>();
            nodeKeyColumns.addAll((Collection<? extends String>) yamlMap.get("node_key_columns"));
            // Assigning nodeCompositeIndexProperties
            nodeCompositeIndexProperties = new ArrayList<String>();
            nodeCompositeIndexProperties
                    .addAll((Collection<? extends String>) yamlMap.get("node_composite_index_properties"));
            // Assigning nodeProperties
            nodeProperties = (List<Map<String, Object>>) yamlMap.get("node_properties");
            // Assigning bridgeProperties
            bridgeProperties = (List<Map<String, Object>>) yamlMap.get("bridge_properties");
            // Assigning causalProperties
            causalProperties = (List<Map<String, Object>>) yamlMap.get("causal_properties");
            // Assigning database Properties file
            databasePropertiesFile = (String) yamlMap.get("db_file");
            // Assigning dataFile from which nodes and bridges will be created
            dataFile = (String) yamlMap.get("data_file");
            // Assigning causalFile from which causal edges will be created
            causalFile = (String) yamlMap.get("causal_file");
            // Assigning batchSize which will control number of graph operation should be
            // commited in bulk
            batchSize = Long.valueOf(yamlMap.get("batch_commit_size").toString());
            // Assigning List of Historical Anomaly Families
            historicalAnomalyFamlies = new ArrayList<String>();
            historicalAnomalyFamlies.addAll((Collection<? extends String>) yamlMap.get("historical_anomaly_families"));

            // Assigning List of List Propertiesin the Node
            listPropertiesInNode = new ArrayList<String>();
            listPropertiesInNode.addAll((Collection<? extends String>) yamlMap.get("list_properties_in_node"));
            
            // Assigning ruleEngine
            ruleEngineForRootCauses = (Map<String, List<String>>) yamlMap.get("rca_rule_engine");
        }

        catch (NullPointerException e) {
            System.out.print(e.getMessage());
            e.printStackTrace();
        }
    }

    public long getBatchSize() {
        return batchSize;
    }

    public ArrayList<String> getNodeKeyColumns() {
        return nodeKeyColumns;
    }

    public ArrayList<String> getHistoricalAnomalyFamilies() {
        return historicalAnomalyFamlies;
    }

    public ArrayList<String> getMultiAnomalyAlgoFamilies() {
        return multiAnomalyAlgoFamilies;
    }

    public ArrayList<String> getSingleAnomalyAlgoFamilies() {
        return singleAnomalyAlgoFamilies;
    }

    public ArrayList<String> getNodeCompositeIndexProperties() {
        return nodeCompositeIndexProperties;
    }

    public List<Map<String, Object>> getNodeProperties() {
        return nodeProperties;
    }

    public List<Map<String, Object>> getBridgeProperties() {
        return bridgeProperties;
    }

    public List<Map<String, Object>> getCausalProperties() {
        return causalProperties;
    }

    public String getDatabasePropertyFile() {
        return databasePropertiesFile;
    }

    public String getDataFile() {
        return dataFile;
    }

    public String getCausalFile() {
        return causalFile;
    }

    public List<String> getListPropertiesInNode(){
        return listPropertiesInNode;
    }

    public Map<String, List<String>> getRuleEngineForRootCauses(){
        return ruleEngineForRootCauses;

    }
}
