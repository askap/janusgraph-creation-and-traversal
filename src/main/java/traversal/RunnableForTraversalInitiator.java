package traversal;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.janusgraph.core.JanusGraph;

public class RunnableForTraversalInitiator implements Runnable {


    private JanusGraph graph;
    private String timeColumn ;
    private int maxDepthForRootCause ;
    private int maxDepthForImpact ;
    private String folderToWriteResultsRootCause;
    private String folderToWriteResultsImpact;
    private int rangeCheckParam;
    private boolean enablePositiveNegativeRangeCheck;
    private String levelName;
    private String theDate;
    private Double filteringVariableThreshold;
    private String anomaly_experiment_id;
   	private String anomaly_run_id;
   	private String causal_experiment_id;
   	private String causal_run_id;
    private String data_source_id;
    private List<String> requiredAnomalyAlgoFamilies;
    private List<String> historicalAnomalyFamilies;
    private Map<String, List<String>> ruleEngineForRootCauses;
    private List<String> requiredKPIs;


    RunnableForTraversalInitiator(JanusGraph graph,String timeColumn,String folderToWriteResultsRootCause,String folderToWriteResultsImpact,int rangeCheckParam
            ,boolean enablePositiveNegativeRangeCheck
            ,int maxDepthForRootCause
            ,int maxDepthForImpact
            ,String levelName
            ,String theDate
            ,Double filteringVariableThreshold, String anomaly_experiment_id, String anomaly_run_id
            ,String causal_experiment_id, String causal_run_id, String data_source_id
            ,List<String> requiredAnomalyAlgoFamilies
            ,List<String> historicalAnomalyFamilies
            ,Map<String, List<String>> ruleEngineForRootCauses
            ,List<String> requiredKPIs) {
		super();
		this.graph = graph;
		this.timeColumn = timeColumn;
		this.maxDepthForRootCause = maxDepthForRootCause;
		this.maxDepthForImpact = maxDepthForImpact;
		this.folderToWriteResultsRootCause = folderToWriteResultsRootCause;
		this.folderToWriteResultsImpact = folderToWriteResultsImpact;
		this.rangeCheckParam = rangeCheckParam;
		this.enablePositiveNegativeRangeCheck = enablePositiveNegativeRangeCheck;
		this.levelName = levelName;
		this.theDate = theDate;
		this.filteringVariableThreshold = filteringVariableThreshold;
		this.anomaly_experiment_id = anomaly_experiment_id;
		this.anomaly_run_id = anomaly_run_id;
		this.causal_experiment_id = causal_experiment_id;
		this.causal_run_id = causal_run_id;
        this.data_source_id = data_source_id;
        this.requiredAnomalyAlgoFamilies = requiredAnomalyAlgoFamilies;
        this.historicalAnomalyFamilies = historicalAnomalyFamilies;
        this.ruleEngineForRootCauses = ruleEngineForRootCauses;
        this.requiredKPIs = requiredKPIs;
	}

    public void run(){

        TraversalInitiatorForOneTimeOneLevel obj = new TraversalInitiatorForOneTimeOneLevel(graph
                                                            ,timeColumn,folderToWriteResultsRootCause
                                                            ,folderToWriteResultsImpact,rangeCheckParam
                                                            ,enablePositiveNegativeRangeCheck
                                                            ,maxDepthForRootCause,maxDepthForImpact
                                                            ,levelName,theDate,filteringVariableThreshold
                                                            ,anomaly_experiment_id
                                                            ,anomaly_run_id,causal_experiment_id,causal_run_id
                                                            ,data_source_id,requiredAnomalyAlgoFamilies
                                                            ,historicalAnomalyFamilies
                                                            ,ruleEngineForRootCauses
                                                            ,requiredKPIs);                
        boolean status = false;
		try {
			status = obj.runTraversal();
		} catch (UnsupportedOperationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        System.out.println("Status for "+levelName+" "+theDate+" : "+status);
        
    }
}