package traversal;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

class ChunkAllocation {
    private int chunkSize;
    private List<HashMap<Long, Integer>> indexToSearch;
    private ListIterator<Map<Long, Integer>> chunkIterator;

    ChunkAllocation(int chunkSize, List<HashMap<Long, Integer>> indexToSearch){
        this.chunkSize = chunkSize;
        this.indexToSearch = indexToSearch;
    }

    private void allocateChunksForGivenIndexToSearch(){
        ArrayList<Map<Long, Integer>> mapList = new ArrayList<Map<Long, Integer>>();
        Map<Long, Integer> smallMap = new HashMap<Long, Integer>();
        int idx = 1;
        System.out.print("\nMap Chunking Started");

        for (int i = 0; i < indexToSearch.size(); i++) {
            for (Map.Entry<Long, Integer> entry : indexToSearch.get(i).entrySet()) {
                smallMap.put(entry.getKey(), entry.getValue());
                idx = idx + 1;
                if (idx % chunkSize == 0) {
                    mapList.add(smallMap);
                    smallMap = new HashMap<Long, Integer>();
                }
            }
        }
        mapList.add(smallMap);
        System.out.print("\nMap Chunking Done. Total Chunks: " + mapList.size());
        chunkIterator = mapList.listIterator();
    }

    public ListIterator<Map<Long, Integer>> getChunkIterator(){
        allocateChunksForGivenIndexToSearch();
        return chunkIterator;
    }



}