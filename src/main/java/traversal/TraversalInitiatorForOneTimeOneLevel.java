package traversal;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.apache.tinkerpop.shaded.jackson.databind.ObjectMapper;
import org.janusgraph.core.JanusGraph;

import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import utils.RCAReportPoJo;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import com.google.gson.Gson;

class TraversalInitiatorForOneTimeOneLevel {

    private JanusGraph graph;
    private String timeColumn ;
    private int maxDepthForRootCause ;
    private int maxDepthForImpact ;
    private String folderToWriteResultsRootCause;
    private String folderToWriteResultsImpact;
    private int rangeCheckParam;
    private boolean enablePositiveNegativeRangeCheck;
    private String levelName;
    private String theDate;
    private ResultClassIndexToSearchAndNodeIds result;
    private Double filteringVariableThreshold;
    private String anomaly_experiment_id;
   	private String anomaly_run_id;
   	private String causal_experiment_id;
   	private String causal_run_id;
    private String data_source_id;
    private List<String> requiredAnomalyAlgoFamilies;
    private List<String> historicalAnomalyFamilies;
    private Map<String, List<String>> ruleEngineForRootCauses;
    private List<String> requiredKPIs;

    

    public TraversalInitiatorForOneTimeOneLevel(JanusGraph graph,String timeColumn,String folderToWriteResultsRootCause,String folderToWriteResultsImpact,int rangeCheckParam
            ,boolean enablePositiveNegativeRangeCheck
            ,int maxDepthForRootCause
            ,int maxDepthForImpact
            ,String levelName
            ,String theDate
            ,Double filteringVariableThreshold, String anomaly_experiment_id, String anomaly_run_id,
            String causal_experiment_id, String causal_run_id, String data_source_id
            ,List<String> requiredAnomalyAlgoFamilies
            ,List<String> historicalAnomalyFamilies
            ,Map<String, List<String>> ruleEngineForRootCauses
            ,List<String> requiredKPIs) {
		super();
		this.graph = graph;
		this.timeColumn = timeColumn;
		this.maxDepthForRootCause = maxDepthForRootCause;
		this.maxDepthForImpact = maxDepthForImpact;
		this.folderToWriteResultsRootCause = folderToWriteResultsRootCause;
		this.folderToWriteResultsImpact = folderToWriteResultsImpact;
		this.rangeCheckParam = rangeCheckParam;
		this.enablePositiveNegativeRangeCheck = enablePositiveNegativeRangeCheck;
		this.levelName = levelName;
		this.theDate = theDate;
		this.filteringVariableThreshold = filteringVariableThreshold;
		this.anomaly_experiment_id = anomaly_experiment_id;
		this.anomaly_run_id = anomaly_run_id;
		this.causal_experiment_id = causal_experiment_id;
		this.causal_run_id = causal_run_id;
        this.data_source_id = data_source_id;
        this.requiredAnomalyAlgoFamilies = requiredAnomalyAlgoFamilies;
        this.historicalAnomalyFamilies = historicalAnomalyFamilies;
        this.ruleEngineForRootCauses = ruleEngineForRootCauses;
        this.requiredKPIs = requiredKPIs;
	}



	Callable<Boolean> callableRootCause = new Callable<Boolean>()
    {
       @Override
       public Boolean call() throws Exception
       {
        try{
            RootCauseTraversal rootCauseObj = new RootCauseTraversal(graph 
                                                                    ,maxDepthForRootCause
                                                                    ,levelName
                                                                    ,theDate
                                                                    ,folderToWriteResultsRootCause
                                                                    ,result.getIndexToSearch()
                                                                    ,rangeCheckParam
                                                                    ,enablePositiveNegativeRangeCheck
                                                                    ,filteringVariableThreshold
                                                                    ,requiredAnomalyAlgoFamilies
                                                                    ,ruleEngineForRootCauses);
            return rootCauseObj.initiateRootCauseTraversal();
        } catch (Exception e){
            System.out.println("Issue with callableRootCause function in TraversalInitiatorForOneTimeOneLevel");
            return false;
            }
        }
    };

    Callable<Boolean> callableImpact = new Callable<Boolean>()
    {
       @Override
       public Boolean call() throws Exception
       {
        try{
            ImpactTraversal impactObj = new ImpactTraversal(graph, maxDepthForImpact, levelName, theDate, folderToWriteResultsImpact, result.getIndexToSearch(), filteringVariableThreshold);
            return impactObj.initiateImpactTraversal();
        } catch (Exception e){
                System.out.println("Issue with callableImpact function in TraversalInitiatorForOneTimeOneLevel");
                return false;
            }
            
        }
    };


    private boolean executeRootCauseAndImpactIdentification(){

        
        
        try{
            IndexForGivenDate indexForGivenDateObj = new IndexForGivenDate(graph, theDate, timeColumn, levelName);
            int the_index = indexForGivenDateObj.getDateIndex();

            if(the_index == -1){
                System.out.println("################# Given Date is not Valid ##############");
                return false;
            }
            else if(the_index == -2){
                System.out.println("################# Given Level is not Valid ##############");
                return false;
            }
            
            
            // Creating a Class Object
            AnomalousNodesForGivenLevelAndDate obj = new AnomalousNodesForGivenLevelAndDate(graph, the_index, levelName, filteringVariableThreshold, requiredAnomalyAlgoFamilies, requiredKPIs);
            result = obj.identifyAnomalousNodes();
            System.out.println("############Done with Anomaly identification##############");

            if (result.getIndexToSearch().size() == 1 && result.getIndexToSearch().get(0).isEmpty()) {
                // System.out.print("No valid anomalous nodes found for the given level and Date");
                return false;
            }
            
            List<Callable<Boolean>> taskList = new ArrayList<Callable<Boolean>>();
            taskList.add(callableRootCause);
            taskList.add(callableImpact);
            
            //create a pool executor with 2 threads
            ExecutorService executor = Executors.newFixedThreadPool(2);

            
            //start the threads and wait for them to finish
            executor.invokeAll(taskList);

            executor.shutdown();

            while (!executor.isTerminated()) {
            }
            
            return true;

        } catch (Exception e){
            e.printStackTrace();
            System.out.println("Issue with executeRootCauseAndImpactIdentification function in TraversalInitiatorForOneTimeOneLevel");
            return false;
        }

        
    }

    public boolean runTraversal() throws UnsupportedOperationException, IOException{
        boolean traversalStatus = executeRootCauseAndImpactIdentification();
        traversalStatus=true;
    	if(traversalStatus) {
            RCAReportPoJo rcaPojo = new RCAReportPoJo();
            rcaPojo.setAnomaly_experiment_id(anomaly_run_id);
            rcaPojo.setAnomaly_project_id(anomaly_experiment_id);
            rcaPojo.setCausal_project_id(causal_experiment_id);
            rcaPojo.setCausal_experiment_id(causal_run_id);
            rcaPojo.setDate_to_run(theDate);
            rcaPojo.setLevelName(levelName);
            String       postUrl       = "http://localhost:5008/api/inference/generate_rca_report/"+data_source_id;
            Gson         gson          = new Gson();
            HttpClient   httpClient    = HttpClientBuilder.create().build();
            HttpPost     post          = new HttpPost(postUrl);
            StringEntity postingString = new StringEntity(gson.toJson(rcaPojo));
            post.setEntity(postingString);
            post.setHeader("Content-type", "application/json");
            HttpResponse  response = httpClient.execute(post);
            System.out.println(EntityUtils.toString(response.getEntity()));

    	}
        return traversalStatus;
    }

}
