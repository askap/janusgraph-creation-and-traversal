package traversal;

import java.util.HashMap;
import java.util.List;

class ResultClassIndexToSearchAndNodeIds {

    private List<HashMap<Long, Integer>> indexToSearch;
    private List<Long> nodeIds;

    ResultClassIndexToSearchAndNodeIds(List<HashMap<Long, Integer>> indexToSearchInput, List<Long> nodeIdsInput) {
        this.indexToSearch = indexToSearchInput;
        this.nodeIds = nodeIdsInput;
    }

    public List<HashMap<Long, Integer>> getIndexToSearch() {
        return indexToSearch;
    }

    public List<Long> getNodeIds() {
        return nodeIds;
    }

}