package traversal;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

class ResultClassSingleChunkTraversalResults {
    
    private Map<String, String> pathResultForCurrentHop;
    private List<HashMap<Long, Integer>> intermediateIndexToSearch;
    private Long sizeCheck;
    private Set<Long> coveredNodeIds;

    ResultClassSingleChunkTraversalResults(Map<String, String> pathResultForCurrentHop
                                            ,List<HashMap<Long, Integer>> intermediateIndexToSearch
                                            ,Long sizeCheck, Set<Long> coveredNodeIds) {

        this.pathResultForCurrentHop = pathResultForCurrentHop;
        this.intermediateIndexToSearch = intermediateIndexToSearch;
        this.sizeCheck = sizeCheck;
        this.coveredNodeIds = coveredNodeIds;
    }

    public Map<String, String> getPathResultForCurrentHop() {
        return pathResultForCurrentHop;
    }

    public List<HashMap<Long, Integer>> getIntermediateIndexToSearch() {
        return intermediateIndexToSearch;
    }

    public Long getSizeCheck() {
        return sizeCheck;
    }

    public Set<Long> getCoveredNodeIds() {
        return coveredNodeIds;
    }

}