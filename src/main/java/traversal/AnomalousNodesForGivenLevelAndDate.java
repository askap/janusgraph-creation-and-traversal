package traversal;

import static org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.__.constant;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.tinkerpop.gremlin.process.traversal.Operator;
import org.apache.tinkerpop.gremlin.process.traversal.Traverser;
import org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.GraphTraversalSource;
import org.apache.tinkerpop.gremlin.process.traversal.step.util.BulkSet;
import org.apache.tinkerpop.gremlin.structure.Vertex;
import org.janusgraph.core.JanusGraph;
import org.janusgraph.core.JanusGraphTransaction;

import utils.SplitStringToListUtil;


class AnomalousNodesForGivenLevelAndDate {

    private JanusGraph graph;
    private int indexToStart;
    private String levelName;
    private Map<String, Object> traversalResult;
    private SplitStringToListUtil parseUtils = new SplitStringToListUtil();
    private Double filteringVariableThreshold;
    private List<String> requiredAnomalyAlgoFamilies;
    private List<String> requiredKPIs;

    AnomalousNodesForGivenLevelAndDate(JanusGraph graph, int indexToStart, String levelName, Double filteringVariableThreshold, List<String> requiredAnomalyAlgoFamilies, List<String> requiredKPIs) {
        this.graph = graph;
        this.indexToStart = indexToStart;
        this.levelName = levelName;
        this.parseUtils = new SplitStringToListUtil();
        this.filteringVariableThreshold = filteringVariableThreshold;
        this.requiredAnomalyAlgoFamilies = requiredAnomalyAlgoFamilies;
        this.requiredKPIs = requiredKPIs;
    }
    AnomalousNodesForGivenLevelAndDate(JanusGraph graph, int indexToStart, String levelName, Double filteringVariableThreshold) {
        this.graph = graph;
        this.indexToStart = indexToStart;
        this.levelName = levelName;
        this.parseUtils = new SplitStringToListUtil();
        this.filteringVariableThreshold = filteringVariableThreshold;
        this.requiredAnomalyAlgoFamilies = null;
        this.requiredKPIs = null;
    }

    private boolean checkAnomalyWithSmartFilteration(Traverser<Vertex> x){

        Integer indexUnderConsideration = (Integer) x.sack();
        // Smart Search Threshold search
        if(filteringVariableThreshold != null){
            // initiaslising things inside because, there is no need for these variable
            // if the filteringVariableThreshold is null
            Double [] filteringVariableValues = parseUtils.parseCommaSeparatedStringToListOfDouble(x.get().value("filtering_variable_value"));
            Double filteringValueUnderConsideration = filteringVariableValues[indexUnderConsideration];
            if(filteringValueUnderConsideration < filteringVariableThreshold){
                return false;
            }
        }

        for (String anomalyFamily : requiredAnomalyAlgoFamilies){
            Integer [] anomalyList = {};
            try{
                anomalyList = parseUtils.parseCommaSeparatedStringToListOfInteger(x.get().value("anomaly_" + anomalyFamily));
            } catch(IllegalArgumentException e){
                // If the given anomaly family is not present in the Property list.
                continue;
            }
            Integer valueToCheck;

            try{
                valueToCheck = anomalyList[indexUnderConsideration];
            } catch(ArrayIndexOutOfBoundsException e){
                // If the current index it self is out of bounds return false (meaning No anomaly found)
                return false;
            }

            if(valueToCheck == 1.0){
                // If current node is anomalous for the given index.
                // Set sack value for the node to be current index and return true
                x.sack(indexUnderConsideration);
                
                return true;
            }

        }

        return false;
    }

    private boolean requiredNodes(Traverser<Vertex> x){
        String kpiName = (String) x.get().value("name");
        if(requiredKPIs.contains(kpiName)){
            return true;
        }
        return false;
    }

    private void traverseGraph() {

        try{
            JanusGraphTransaction tnx = graph.newTransaction();
            GraphTraversalSource g = tnx.traversal();

            try {
                if(requiredKPIs != null){
                traversalResult = g.V().has("level", levelName)
                                    .filter(x -> requiredNodes(x))
                                    .sack(Operator.assign).by(constant(indexToStart))
                                    .filter(x -> checkAnomalyWithSmartFilteration(x)).as("nodes")
                                    .select("nodes")
                                    .aggregate("final_nodes")
                                    .sack().fold().as("sack")
                                    .select("final_nodes", "sack").next();
                }
                else{
                    traversalResult = g.V().has("level", levelName)
                                    .sack(Operator.assign).by(constant(indexToStart))
                                    .filter(x -> checkAnomalyWithSmartFilteration(x)).as("nodes")
                                    .select("nodes")
                                    .aggregate("final_nodes")
                                    .sack().fold().as("sack")
                                    .select("final_nodes", "sack").next();

                }

                

            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("Issue with anomaly detection query in traverseGraph function in AnomalousNodesForGivenLevelAndDate");
            }
            finally{
                // Close the graph traversal g
                try {
                    g.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                // Rollback and close the active transaction
                tnx.rollback();
                tnx.close();
            }
        } catch (Exception e){
            System.out.println("Issue with traverseGraph function in AnomalousNodesForGivenLevelAndDate");
            e.printStackTrace();
        }
    }

    private ResultClassIndexToSearchAndNodeIds processTraversalResult() {

        BulkSet traversedPaths = (BulkSet) traversalResult.get("final_nodes");
        ArrayList sack = (ArrayList) traversalResult.get("sack");
        Iterator traversedPathsIterator = traversedPaths.iterator();

        Vertex v;
        int counter = 0;

        List<HashMap<Long, Integer>> indexToSearch = new ArrayList<HashMap<Long, Integer>>();
        List<Long> nodeIds = new ArrayList<>();

        indexToSearch.add(new HashMap<Long, Integer>());

        while (traversedPathsIterator.hasNext()) {
            v = (Vertex) traversedPathsIterator.next();
            indexToSearch.get(0).put((Long) v.id(), (Integer) sack.get(counter));
            nodeIds.add((Long) v.id());
            counter = counter + 1;
        }

        ResultClassIndexToSearchAndNodeIds resultToReturn = new ResultClassIndexToSearchAndNodeIds(indexToSearch, nodeIds);

        return resultToReturn;

    }

    public ResultClassIndexToSearchAndNodeIds identifyAnomalousNodes() {

        traverseGraph();

        ResultClassIndexToSearchAndNodeIds resultToReturn = processTraversalResult();

        return resultToReturn;
    }

}