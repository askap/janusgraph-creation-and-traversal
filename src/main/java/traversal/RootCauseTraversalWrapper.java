package traversal;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.tinkerpop.shaded.jackson.core.JsonParseException;
import org.apache.tinkerpop.shaded.jackson.databind.JsonMappingException;
import org.apache.tinkerpop.shaded.jackson.databind.ObjectMapper;
import org.janusgraph.core.JanusGraph;
import org.janusgraph.core.JanusGraphFactory;

import app.RCATraversalDto;
import utils.YamlFileProcessorUtil;

public class RootCauseTraversalWrapper {


    private String db_file;
    private JanusGraph graph;
    private String theDate ;
    private String timeColumn ;
    private String levelName;
    private int maxDepth ;
    private String folderToWriteResults;
    private int rangeCheckParam;
    private boolean enablePositiveNegativeRangeCheck;
    private Double filteringVariableThreshold;
    private List<String> requiredAnomalyAlgoFamilies = new ArrayList<String>();
    private Map<String, List<String>> ruleEngineForRootCauses = new HashMap<String, List<String>>();
    private Map<String, Object> jsonToParamsMap(String jsonString){

        ObjectMapper mapper = new ObjectMapper();
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            map = mapper.readValue(jsonString, Map.class);
            System.out.print(map);
        } catch (JsonParseException e) {
            e.printStackTrace();
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return map;

    }

    private boolean startGraph(){
        try{
            graph = JanusGraphFactory.open(this.db_file);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Graph Not Started Successfully");
            return false;
        }
        return true;
    }
    
    public RootCauseTraversalWrapper(RCATraversalDto rcaDto){

        String config_file = rcaDto.getConfig_file();
    	String the_date = rcaDto.getThe_date();
    	String time_column= rcaDto.getTime_column();
    	Integer max_depth= rcaDto.getMax_depth();
    	String level_name= rcaDto.getLevel_name();
    	String write_folder= rcaDto.getWrite_folder();
        Integer range_for_anomaly= rcaDto.getRange_for_anomaly();
        Double filteringVariableThreshold = rcaDto.getFilteringVariableThreshold();
    	
        try {
        	
        	
            YamlFileProcessorUtil yamlObj = new YamlFileProcessorUtil(config_file);
            yamlObj.getDatabasePropertyFile();
            this.db_file = yamlObj.getDatabasePropertyFile();

            this.theDate = the_date;
            this.timeColumn = time_column;
            this.levelName = level_name;
            this.maxDepth = max_depth;
            this.folderToWriteResults = write_folder;
        } catch (NullPointerException e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
        try {
            this.rangeCheckParam = range_for_anomaly;
        } catch (NullPointerException e) {
            this.rangeCheckParam = 0;
        }
        try {
            this.enablePositiveNegativeRangeCheck = false; //(boolean) paramsMap.get("pos_neg_range_check");
        }
        catch (NullPointerException e) {
            this.enablePositiveNegativeRangeCheck = false;
        }
        this.filteringVariableThreshold = filteringVariableThreshold;

    }

    public boolean runRootCauseTraversal(){

        boolean graphStartStatus = startGraph();
        if(graphStartStatus == false){
            return false;
        }

        try{
            IndexForGivenDate indexForGivenDateObj = new IndexForGivenDate(graph, theDate, timeColumn, levelName);
            int the_index = indexForGivenDateObj.getDateIndex();

            if(the_index == -1){
                System.out.println("################# Given Date is not Valid ##############");
                return false;
            }
            else if(the_index == -2){
                System.out.println("################# Given Level is not Valid ##############");
                return false;
            }
            
            
            // Creating a Class Object
            AnomalousNodesForGivenLevelAndDate obj = new AnomalousNodesForGivenLevelAndDate(graph, the_index, levelName, filteringVariableThreshold);
            ResultClassIndexToSearchAndNodeIds result = obj.identifyAnomalousNodes();
            System.out.println("IndexToSearch");
            System.out.println(result.getIndexToSearch());
            System.out.println("nodeIds");
            System.out.println(result.getNodeIds());
            System.out.println("############Done with Anomaly identification##############");

            RootCauseTraversal rootCauseObj = new RootCauseTraversal(graph, maxDepth, levelName, theDate,folderToWriteResults,result.getIndexToSearch(),rangeCheckParam, enablePositiveNegativeRangeCheck, filteringVariableThreshold,requiredAnomalyAlgoFamilies,ruleEngineForRootCauses);
            
            boolean traversalStatus = rootCauseObj.initiateRootCauseTraversal();
            if(traversalStatus == false){
                return false;
            }

            System.out.println("############Done with Root Cause Analysis###############");
            return true;

        }catch (Exception e){
            e.printStackTrace();
            return false;
        } finally {
            graph.close();
        }


    }

}