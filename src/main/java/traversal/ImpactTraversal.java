package traversal;


import org.apache.tinkerpop.gremlin.structure.io.graphson.GraphSONMapper;
import org.apache.tinkerpop.gremlin.structure.io.graphson.GraphSONVersion;
import org.apache.tinkerpop.shaded.jackson.databind.ObjectMapper;
import org.janusgraph.core.JanusGraph;
import java.util.HashMap;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.lang.System;


class ImpactTraversal {

    private JanusGraph graph;
    private int maxDepth;
    private List<HashMap<Long, Integer>> indexToSearch;
    private ObjectMapper mapper = GraphSONMapper.build().version(GraphSONVersion.V1_0).create().createMapper();
    private int chunkSize = 100;
    private String folderToWriteResults;
    private String dateOfInterest;
    private String levelOfInterest;
    private Double filteringVariableThreshold;

    ImpactTraversal(JanusGraph graph
                    ,int maxDepth
                    ,String levelOfInterest
                    ,String dateOfInterest
                    ,String folderToWriteResults
                    ,List<HashMap<Long, Integer>> indexToSearch
                    ,Double filteringVariableThreshold) {
        this.graph = graph;
        this.maxDepth = maxDepth;
        this.indexToSearch = indexToSearch;
        this.folderToWriteResults = folderToWriteResults;
        this.levelOfInterest = levelOfInterest;
        this.dateOfInterest = dateOfInterest;
        this.filteringVariableThreshold = filteringVariableThreshold;
    }

    private void getImpactTraversalPathsAsHopWiseJson() {
        long startTime;
        long endTime;

        // firstIndex has been added to support the elimination for edges(specifically Causal) with 0 beta
        // in Single ChunkImpactTraversal, since our reference point is the starting date for Impact Traversal
        Integer firstIndex = (Integer)indexToSearch.get(0).values().toArray()[0];
        
    

        for (int hop = 0; hop < maxDepth; hop++) {
            if (indexToSearch.size() == 1 && indexToSearch.get(0).isEmpty()) {
                System.out.print("No valid anomalous nodes found in the hop");
                break;
            }

            Map<String, String> pathResultForCurrentHop = new HashMap<String, String>();

            startTime = System.currentTimeMillis();
            System.out.print("\n********************************************************\n");
            System.out.print("\nProcessing Hop: " + hop);
            List<HashMap<Long, Integer>> intermediateIndexToSearch = new ArrayList<HashMap<Long, Integer>>();

            // if indexToSearch is too large, we divide it into chunks of size as defined by
            // chunkSize

            
            ChunkAllocation chunkAllocationObj = new ChunkAllocation(chunkSize, indexToSearch);
            ListIterator<Map<Long, Integer>> iterator = chunkAllocationObj.getChunkIterator();
            System.out.print("\nProcessing Map Chunks");
            Long sizeCheck = 0L;

            int idx = 1;
            while (iterator.hasNext()) {
                System.out.print("," + idx);
                idx = idx + 1;

                Map<Long, Integer> indexMap = iterator.next();
                try{
                SingleChunkImpactTraversal singleChunkTraversalObj = 
                            new SingleChunkImpactTraversal(graph
                                                            ,indexMap
                                                            ,firstIndex
                                                            ,pathResultForCurrentHop
                                                            ,intermediateIndexToSearch
                                                            ,sizeCheck
                                                            ,mapper,filteringVariableThreshold);
                
                ResultClassSingleChunkTraversalResults resultObj = singleChunkTraversalObj.getSingleChunkImpactResult();
                pathResultForCurrentHop = resultObj.getPathResultForCurrentHop();
                intermediateIndexToSearch = resultObj.getIntermediateIndexToSearch();
                sizeCheck = resultObj.getSizeCheck();
                } catch(Exception e){
                    continue;
            }
                
            }

            System.out.println("\nIntermediate Result Stats: \n");
            System.out.println("Result Size: "+intermediateIndexToSearch.size()+" Check size: "+sizeCheck);
            //System.out.print("Intermediate Result: \n"+ intermediateIndexToSearch);
            indexToSearch = new ArrayList<HashMap<Long, Integer>>();
            //This indexToSearch will be considered for next hop
            indexToSearch = intermediateIndexToSearch;
            
            endTime = System.currentTimeMillis();
            System.out.println("\n\nTime taken for Traversal for Depth "+(hop+1)+": " + (endTime - startTime)/1000 + " seconds");
            

            if(indexToSearch.size() == 0){
                System.out.print("\n################## Done with the process #################");
                break;

            }

            System.out.println("\n\nTime taken for Depth including post processing "+(hop+1)+": " + (System.currentTimeMillis() - startTime)/1000 + " seconds");
            
            
            JsonPathWritter jsonWriteObj = new JsonPathWritter(folderToWriteResults, levelOfInterest, dateOfInterest, "HOP",hop, pathResultForCurrentHop);
            jsonWriteObj.writePathtoJson();
            

        }
        
    }

    public boolean initiateImpactTraversal(){
        try{
            getImpactTraversalPathsAsHopWiseJson();
            return true;
        }catch(Exception e){
            System.out.println("Issue with ImpactTraversal");
            e.printStackTrace();
            return false;
        }

    }

}