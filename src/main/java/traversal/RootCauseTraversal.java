package traversal;

import org.apache.tinkerpop.gremlin.structure.io.graphson.GraphSONMapper;
import org.apache.tinkerpop.gremlin.structure.io.graphson.GraphSONVersion;
import org.apache.tinkerpop.shaded.jackson.databind.ObjectMapper;
import org.janusgraph.core.JanusGraph;

import java.util.HashMap;
import java.util.HashSet;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;
import java.lang.System;

class RootCauseTraversal {

    private JanusGraph graph;
    private int maxDepth;
    private List<HashMap<Long, Integer>> indexToSearch;
    private ObjectMapper mapper = GraphSONMapper.build().version(GraphSONVersion.V1_0).create().createMapper();
    private int chunkSize = 100;
    private String folderToWriteResults;
    private String dateOfInterest;
    private String levelOfInterest;
    private int rangeCheckParam;
    private boolean enablePositiveNegativeRangeCheck;
    private Double filteringVariableThreshold;
    private List<String> requiredAnomalyAlgoFamilies;
    private Map<String, List<String>> ruleEngineForRootCauses;



    RootCauseTraversal(JanusGraph graph, int maxDepth, String levelOfInterest, String dateOfInterest,
            String folderToWriteResults, List<HashMap<Long, Integer>> indexToSearch, int rangeCheckParam,
            boolean enablePositiveNegativeRangeCheck, Double filteringVariableThreshold,
            List<String> requiredAnomalyAlgoFamilies,Map<String, List<String>> ruleEngineForRootCauses) {
        this.graph = graph;
        this.maxDepth = maxDepth;
        this.indexToSearch = indexToSearch;
        this.folderToWriteResults = folderToWriteResults;
        this.levelOfInterest = levelOfInterest;
        this.dateOfInterest = dateOfInterest;
        this.rangeCheckParam = rangeCheckParam;
        this.enablePositiveNegativeRangeCheck = enablePositiveNegativeRangeCheck;
        this.filteringVariableThreshold = filteringVariableThreshold;
        this.requiredAnomalyAlgoFamilies = requiredAnomalyAlgoFamilies;
        this.ruleEngineForRootCauses = ruleEngineForRootCauses; 

    }

    private void getRootCauseTraversalPathsAsHopWiseJson() {
        long startTime;
        long endTime;

        

        for (int hop = 0; hop < maxDepth; hop++){
            if (indexToSearch.size() == 1 && indexToSearch.get(0).isEmpty()) {
                System.out.println("No valid anomalous nodes found in the hop");
                break;
            }

            Map<String, String> pathResultForCurrentHop = new HashMap<String, String>();

            startTime = System.currentTimeMillis();
            System.out.println("********************************************************");
            System.out.println("Processing Hop: " + hop);
            List<HashMap<Long, Integer>> intermediateIndexToSearch = new ArrayList<HashMap<Long, Integer>>();

            // if indexToSearch is too large, we divide it into chunks of size as defined by
            // chunkSize

            ChunkAllocation chunkAllocationObj = new ChunkAllocation(chunkSize, indexToSearch);
            ListIterator<Map<Long, Integer>> iterator = chunkAllocationObj.getChunkIterator();
            System.out.print("\nProcessing Map Chunks");
            Long sizeCheck = 0L;

            int idx = 1;
            while (iterator.hasNext()) {
                System.out.print("," + idx);
                idx = idx + 1;

                Map<Long, Integer> indexMap = iterator.next();
                Set<Long> coveredNodeIds = new HashSet<>();
                
                try{
                SingleChunkRootCauseTraversal singleChunkTraversalObj = new SingleChunkRootCauseTraversal(graph,
                        indexMap, pathResultForCurrentHop, intermediateIndexToSearch, sizeCheck, mapper,
                        coveredNodeIds, rangeCheckParam, enablePositiveNegativeRangeCheck,
                        filteringVariableThreshold, requiredAnomalyAlgoFamilies,ruleEngineForRootCauses);
            
                ResultClassSingleChunkTraversalResults resultObj = singleChunkTraversalObj
                        .getSingleChunkRootCauseResult();
                pathResultForCurrentHop = resultObj.getPathResultForCurrentHop();
                intermediateIndexToSearch = resultObj.getIntermediateIndexToSearch();
                sizeCheck = resultObj.getSizeCheck();
                } catch(Exception e){
                    continue;
                }

            }

            System.out.println("\nIntermediate Result Stats: \n");
            System.out.println("Result Size: " + intermediateIndexToSearch.size() + " Check size: " + sizeCheck);
            
            indexToSearch = new ArrayList<HashMap<Long, Integer>>();
            // This indexToSearch will be considered for next hop
            indexToSearch = intermediateIndexToSearch;

            endTime = System.currentTimeMillis();
            System.out.println("\n\nTime taken for Traversal for Depth " + (hop + 1) + ": "
                    + (endTime - startTime) / 1000 + " seconds");

            if (indexToSearch.size() == 0) {
                System.out.println("################## Done with the process #################");
                break;

            }

            System.out.println("\n\nTime taken for Depth including post processing " + (hop + 1) + ": "
                    + (System.currentTimeMillis() - startTime) / 1000 + " seconds");
            
            JsonPathWritter jsonWriteObj = new JsonPathWritter(folderToWriteResults, levelOfInterest,
                    dateOfInterest, "HOP", hop, pathResultForCurrentHop);
            jsonWriteObj.writePathtoJson();
        }

    }

    public boolean initiateRootCauseTraversal(){
        try{
            getRootCauseTraversalPathsAsHopWiseJson();
            return true;
        } catch(Exception e){
            System.out.println("Issue with RootCauseTraversal");
            e.printStackTrace();
            return false;
        }

    }

}