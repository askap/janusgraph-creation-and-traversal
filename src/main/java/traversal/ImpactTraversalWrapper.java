package traversal;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.tinkerpop.shaded.jackson.core.JsonParseException;
import org.apache.tinkerpop.shaded.jackson.databind.JsonMappingException;
import org.apache.tinkerpop.shaded.jackson.databind.ObjectMapper;
import org.janusgraph.core.JanusGraph;
import org.janusgraph.core.JanusGraphFactory;

import utils.YamlFileProcessorUtil;

public class ImpactTraversalWrapper {
    private String db_file;
    private String theDate;
    private String timeColumn;
    private String levelName;
    private int maxDepth;
    private String folderToWriteResults;
    private JanusGraph graph;
    // TODO There is no connection to filteringVariableThreshold since ImpactTraversalWrapper doesnt have DTO and Json structure doesnt contain it
    private Double filteringVariableThreshold;

    private boolean startGraph(){
        try{
            graph = JanusGraphFactory.open(this.db_file);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Graph Not Started Successfully");
            return false;
        }
        return true;
    }

    private Map<String, Object> jsonToParamsMap(String jsonString) {

        ObjectMapper mapper = new ObjectMapper();
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            map = mapper.readValue(jsonString, Map.class);
            System.out.print(map);
        } catch (JsonParseException e) {
            e.printStackTrace();
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return map;

    }

    public ImpactTraversalWrapper(String paramsJsonString) {
        Map<String, Object> paramsMap = this.jsonToParamsMap(paramsJsonString);
        try {
            YamlFileProcessorUtil yamlObj = new YamlFileProcessorUtil((String) paramsMap.get("config_file"));
            yamlObj.getDatabasePropertyFile();
            this.db_file = yamlObj.getDatabasePropertyFile();

            this.theDate = (String) paramsMap.get("the_date");
            this.timeColumn = (String) paramsMap.get("time_column");
            this.levelName = (String) paramsMap.get("level_name");
            this.maxDepth = (int) paramsMap.get("max_depth");
            this.folderToWriteResults = (String) paramsMap.get("write_folder");
        } catch (NullPointerException e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
        this.filteringVariableThreshold = null;
    }

    ImpactTraversalWrapper(String paramsJsonString, JanusGraph graph) {
        Map<String, Object> paramsMap = this.jsonToParamsMap(paramsJsonString);
        try {
            YamlFileProcessorUtil yamlObj = new YamlFileProcessorUtil((String)paramsMap.get("config_file"));
            yamlObj.getDatabasePropertyFile();
            this.db_file = yamlObj.getDatabasePropertyFile();


            this.theDate = (String)paramsMap.get("the_date");
            this.timeColumn = (String)paramsMap.get("time_column");
            this.levelName = (String)paramsMap.get("level_name");
            this.maxDepth = (int) paramsMap.get("max_depth");
            this.folderToWriteResults = (String)paramsMap.get("write_folder");
        } catch(NullPointerException e){
            System.out.println(e.getMessage());
            e.printStackTrace();
        } 
        this.graph = graph;
    }

    public boolean runImpactAnalysis(){

        boolean graphStartStatus = startGraph();
        if(graphStartStatus == false){
            return false;
        }

        try{
            IndexForGivenDate indexForGivenDateObj = new IndexForGivenDate(graph, theDate, timeColumn, levelName);
            int the_index = indexForGivenDateObj.getDateIndex();

            if(the_index == -1){
                System.out.println("################# Given Date is not Valid ##############");
                return false;
            }
            else if(the_index == -2){
                System.out.println("################# Given Level is not Valid ##############");
                return false;
            }
            
            
            // Creating a Class Object
            AnomalousNodesForGivenLevelAndDate obj = new AnomalousNodesForGivenLevelAndDate(graph, the_index, levelName, filteringVariableThreshold);
            ResultClassIndexToSearchAndNodeIds result = obj.identifyAnomalousNodes();

            ImpactTraversal impactObj = new ImpactTraversal(graph, maxDepth, levelName, theDate,folderToWriteResults,result.getIndexToSearch(), filteringVariableThreshold);
            
            boolean traversalStatus = impactObj.initiateImpactTraversal();
            if(traversalStatus == false){
                return false;
            }
            System.out.println("############Done with Impact Analysis##############");
            
            return true;
        }catch (Exception e){
            return false;
        } finally {
            graph.close();
        }


    }


}