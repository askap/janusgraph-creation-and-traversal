package traversal;

import org.apache.tinkerpop.gremlin.process.traversal.step.util.BulkSet;
import org.apache.tinkerpop.gremlin.process.traversal.step.util.MutablePath;
import org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.GraphTraversalSource;
import org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.__;
import org.apache.tinkerpop.gremlin.structure.Edge;
import org.apache.tinkerpop.gremlin.structure.T;
import org.apache.tinkerpop.gremlin.structure.Vertex;
import org.apache.tinkerpop.gremlin.process.traversal.Operator;
import org.apache.tinkerpop.gremlin.process.traversal.Path;
import org.apache.tinkerpop.gremlin.process.traversal.Traverser;
import org.apache.tinkerpop.shaded.jackson.core.JsonProcessingException;
import org.apache.tinkerpop.shaded.jackson.databind.ObjectMapper;
import org.janusgraph.core.JanusGraph;
import org.janusgraph.core.JanusGraphTransaction;

import groovyjarjarantlr.collections.Stack;

import java.util.HashMap;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.lang.System;
import utils.SplitStringToListUtil;
import utils.YamlFileProcessorUtil;

class SingleChunkRootCauseTraversal {

    private JanusGraph graph;
    private Map<Long, Integer> indexMap;
    private Map<String, String> pathResultForCurrentHop;
    private List<HashMap<Long, Integer>> intermediateIndexToSearch;
    private Long sizeCheck;
    private ObjectMapper mapper;
    private Set<Long> coveredNodeIds;
    private int rangeCheckParam;
    private boolean enablePositiveNegativeRangeCheck;
    private SplitStringToListUtil parseUtils = new SplitStringToListUtil();
    private ArrayList<String> historicalAnomalyFamilies;
    private Double filteringVariableThreshold;
    private List<String> requiredAnomalyAlgoFamilies;
    private Map<String, List<String>> ruleEngineForRootCauses;

    SingleChunkRootCauseTraversal(JanusGraph graph, Map<Long, Integer> indexMap,
            Map<String, String> pathResultForCurrentHop, List<HashMap<Long, Integer>> intermediateIndexToSearch,
            Long sizeCheck, ObjectMapper mapper, Set<Long> coveredNodeIds, int rangeCheckParam,
            boolean enablePositiveNegativeRangeCheck, Double filteringVariableThreshold,
            List<String> requiredAnomalyAlgoFamilies,
            Map<String, List<String>> ruleEngineForRootCauses) {
        this.graph = graph;
        this.indexMap = indexMap;
        this.pathResultForCurrentHop = pathResultForCurrentHop;
        this.intermediateIndexToSearch = intermediateIndexToSearch;
        this.sizeCheck = sizeCheck;
        this.mapper = mapper;
        this.coveredNodeIds = coveredNodeIds;
        this.rangeCheckParam = rangeCheckParam;
        this.enablePositiveNegativeRangeCheck = enablePositiveNegativeRangeCheck;
        this.parseUtils = new SplitStringToListUtil();
        this.filteringVariableThreshold = filteringVariableThreshold;
        this.requiredAnomalyAlgoFamilies = requiredAnomalyAlgoFamilies;
        this.ruleEngineForRootCauses = ruleEngineForRootCauses;

    }

    
    private boolean checkAnomalyWithSmartFilteration(Traverser<Vertex> x, String anomalyFamily,boolean rootCauseFilteration) {

        Integer indexUnderConsideration = (Integer) x.sack();

        // Smart Search Threshold search
        if (filteringVariableThreshold != null) {
            Double[] filteringVariableValues = parseUtils
                    .parseCommaSeparatedStringToListOfDouble(x.get().value("filtering_variable_value"));
            Double filteringValueUnderConsideration = filteringVariableValues[indexUnderConsideration];
            if (filteringValueUnderConsideration < filteringVariableThreshold) {
                return false;
            }
        }

        if(rootCauseFilteration == false){
            Integer[] anomalyList = parseUtils.parseCommaSeparatedStringToListOfInteger(x.get().value("anomaly_"+anomalyFamily));

            Integer valueToCheck;

            try {
                valueToCheck = anomalyList[indexUnderConsideration];
            } catch (ArrayIndexOutOfBoundsException e) {
                // If the current index it self is out of bounds return false (meaning No
                // anomaly found)
                return false;
            }

            if (valueToCheck == 1.0) {
                // If current node is anomalous for the given index.
                // Set sack value for the node to be current index and return true
                x.sack(indexUnderConsideration);
                return true;
            }
        }

        else{
            // Range Loopup runs only for nodes who's anomaly family is a part of
            // historicalAnomalyFamilies
            if(ruleEngineForRootCauses.containsKey(anomalyFamily)){
                for (String rootCauseAnomalyFamily : ruleEngineForRootCauses.get(anomalyFamily)){
                    Integer[] anomalyList;
                    try{
                        anomalyList = parseUtils.parseCommaSeparatedStringToListOfInteger(x.get().value("anomaly_"+rootCauseAnomalyFamily));
                    } catch (Exception e) {
                        // If the possible Root cause anomaly family doesn't exist from the Rule Engine
                        // go for next family
                        continue;
                    }
                    Integer valueToCheck;

                    try {
                        valueToCheck = anomalyList[indexUnderConsideration];
                    } catch (ArrayIndexOutOfBoundsException e) {
                        // If the current index it self is out of bounds return false (meaning No
                        // anomaly found)
                        return false;
                    }

                    if (valueToCheck == 1.0) {
                        // If current node is anomalous for the given index.
                        // Set sack value for the node to be current index and return true
                        x.sack(indexUnderConsideration);
                        return true;
                    }

                    if (rangeCheckParam > 0 && historicalAnomalyFamilies.contains(rootCauseAnomalyFamily)) {
                        for (int range = 0; range < rangeCheckParam; range++) {
                            indexUnderConsideration = indexUnderConsideration - 1;
                            try {
                                valueToCheck = anomalyList[indexUnderConsideration];
                            } catch (ArrayIndexOutOfBoundsException e) {
                                // If the current index it self is out of bounds return false (meaning No
                                // anomaly found)
                                return false;
                            }
                            if (valueToCheck == 1.0) {
                                // If current node is anomalous for the given index.
                                // Set sack value for the node to be current index and return true
                                x.sack(indexUnderConsideration);
                                return true;
                            }
                        }
                    }

                }
            }
            
        }
        
        return false;

    }

    private boolean filterOutZeroBetasForCausal(Traverser<Edge> x){

        /**
         * This function is used to filter out the edges with beta for the current date as 0.
         * This is focused for Causal edge only, but we dont differentiate since beta for Bridges are always 1.0
         * This will optimse the traversal, since unwanted causals are removed
         * 
         * for Root Cause Traversal specifically the index for the given date is changes as we encounter the lag.
         * this we get from the gremlin sack.
         */

        try{
            Integer indexUnderConsideration = (Integer) x.sack();
            Double[] betaValues = parseUtils
                        .parseCommaSeparatedStringToListOfDouble(x.get().value("betas"));
            Double betaValueUnderConsideration = betaValues[indexUnderConsideration];

            if(betaValueUnderConsideration == 0.0){
                return false;
            }
            else{
                return true;
            }
        } catch (Exception e){
            // Is some issues occur then return false
            return false;
        }

        
    }

    private void processSingleChunk() {
       
        List<Long> nodeIds = new ArrayList<>(indexMap.keySet());
        /*
         * The below query is used to identify the root causes in one hop from the
         * previous level It returns a map { "final_path": [path_1, path_2, ...], "sack"
         * : [0,0,....] } "final_nodes"-> List of all paths "sack"-> the index under
         * consideration for the end node in each path
         */

        JanusGraphTransaction tnx = graph.newTransaction();
        GraphTraversalSource g = tnx.traversal();
        YamlFileProcessorUtil yamlObj = new YamlFileProcessorUtil("/graph_config.yaml");
        historicalAnomalyFamilies = yamlObj.getHistoricalAnomalyFamilies();
        
        // Getting the list properties and adding time property
        List<String> listProperties=yamlObj.getListPropertiesInNode();
        listProperties.add("time");

        

        List<BulkSet> traversedPathsForAllAnomalyFamilies = new ArrayList<BulkSet>();
        List<ArrayList> sackForAllAnomalyFamilies = new ArrayList<ArrayList>();


        Map<String, Object> intermediate_result_chunk = new HashMap<String, Object>();
        
        try {
            for(String anomalyFamily:requiredAnomalyAlgoFamilies){
            
                intermediate_result_chunk = g.withSideEffect("index", indexMap).V(nodeIds)
                                            .sack(Operator.assign)
                                            .by(__.map(s -> ((Map) s.sideEffects("index")).get(((Vertex) s.get()).id())))
                                            .filter(anomaly -> checkAnomalyWithSmartFilteration(anomaly, anomalyFamily,false))
                                            .inE()
                                            .filter(e -> filterOutZeroBetasForCausal(e))
                                            .sack(Operator.minus).by("lag")
                                            .outV()
                                            .filter(rootcause -> checkAnomalyWithSmartFilteration(rootcause, anomalyFamily,true))
                                            .path()
                                            .by(__.valueMap(true)).aggregate("final_paths")
                                            .sack().fold().as("sack")
                                            .select("final_paths", "sack")
                                            .next();
            

            ArrayList sackTracker = (ArrayList) intermediate_result_chunk.get("sack");
            if(sackTracker.size() == 0){
                continue;
            }
            traversedPathsForAllAnomalyFamilies.add((BulkSet) intermediate_result_chunk.get("final_paths"));
            sackForAllAnomalyFamilies.add((ArrayList) intermediate_result_chunk.get("sack"));

            }

        }catch (Exception e) {
            System.out.println("Issue with Single Chunk Root Cause Query");
            e.printStackTrace();
        } finally {
            try {
                g.close();
            } catch (Exception e1) {
                e1.printStackTrace();
            }
            tnx.rollback();
            tnx.close();
        }
        
        /*
         * The below steps are to post process the output we got it creates
         * intermediateIndexToSearch = {node_id:sack}
         * 
         */
        
        // BulkSet traversedPaths = new BulkSet();
        // traversedPaths.addAll(traversedPathsForAllAnomalyFamilies);
        
        HashMap<Long, Integer> nodeIndexTracker = new HashMap<Long, Integer>();
        for (int i  = 0; i<traversedPathsForAllAnomalyFamilies.size();i++){

            BulkSet traversedPaths = traversedPathsForAllAnomalyFamilies.get(i);
            Iterator traversedPathsIterator = traversedPaths.iterator();

            ArrayList sack = sackForAllAnomalyFamilies.get(i);

            sizeCheck = sizeCheck + traversedPaths.longSize();

            int counter = 0;
            while (traversedPathsIterator.hasNext()) {
                MutablePath thePath = (MutablePath) traversedPathsIterator.next();
                
                Long start_v_id = (Long) ((LinkedHashMap) thePath.get(0)).get(T.id);
                Long end_v_id = (Long) ((LinkedHashMap) thePath.get(thePath.size() - 1)).get(T.id);
                
                // Initialising the start and the end node 
                
                
                Integer existingIndex = 0;
                LinkedHashMap start_node=(LinkedHashMap) thePath.get(0);
                LinkedHashMap end_node=(LinkedHashMap) thePath.get(thePath.size() - 1);
                LinkedHashMap edge=(LinkedHashMap) thePath.get(1);

                
                
                if (nodeIndexTracker.containsKey((Long) end_v_id)) {
                    existingIndex = nodeIndexTracker.get((Long) end_v_id);
                    if ((Integer) sack.get(counter) != (Integer) intermediateIndexToSearch.get(existingIndex).get((Long) end_v_id)) {
                        existingIndex = existingIndex + 1;
                    }
                }
                nodeIndexTracker.put((Long) end_v_id, existingIndex);

                if (intermediateIndexToSearch.size() < (existingIndex + 1)) {
                    intermediateIndexToSearch.add(new HashMap<Long, Integer>());
                }

                intermediateIndexToSearch.get(existingIndex).put((Long) end_v_id, (Integer) sack.get(counter));
                

                // pathResultForCurrentHop allocation
                // Getting the path ID for a single hop
                Long before_sack = new Long(indexMap.get(start_v_id));
                Long after_sack = Long.valueOf(sack.get(counter).toString());
                ArrayList<Long> mapKeys = new ArrayList<Long>();

                mapKeys.add(before_sack);
                mapKeys.add(start_v_id);
                mapKeys.add(after_sack);
                mapKeys.add(end_v_id);
                int index_number=before_sack.intValue();
                
                // From the given node getting only those property for the given index particular index 
                // For edege 
                String betas="betas";
                String output_to_process_edge=(String)edge.get(betas);
                ArrayList<String> edge_list=new ArrayList<String>();
                if (output_to_process_edge!=null){
                output_to_process_edge = output_to_process_edge.replace("[","");
                output_to_process_edge = output_to_process_edge.replace("]","");
                List<String> myList_edge = new ArrayList<String>(Arrays.asList(output_to_process_edge.split(",")));
                String final_value_edge =myList_edge.get(index_number);
                final_value_edge="["+final_value_edge+"]";
                edge_list.add(final_value_edge);
               
                edge.put(betas,edge_list);
                

             }
                

                for  (int j = 0; j < listProperties.size(); j++){
                        String strTemp=listProperties.get(j);
                        

                        List output_to_process_start=(List)start_node.get(strTemp);
                        List output_to_process_end=(List)end_node.get(strTemp);
                        ArrayList<String> start_node_list=new ArrayList<String>();
                        ArrayList<String> end_node_list=new ArrayList<String>();
                        
                        if (output_to_process_start==null && output_to_process_end==null){
                            continue;
                        }   
                        
                        if (strTemp.contains("addnl_info")){
                            String addnl_info_start=output_to_process_start.get(0).toString();
                            addnl_info_start = addnl_info_start.replace("[","");
                            addnl_info_start = addnl_info_start.replace("]","");
                            List<String> addnl_info_list_start = new ArrayList<String>(Arrays.asList(addnl_info_start.split("},")));
                            String addnl_info_value_start =addnl_info_list_start.get(index_number);
                            addnl_info_value_start="["+addnl_info_value_start+"}]";
                            start_node_list.add(addnl_info_value_start);
                            start_node.put(strTemp,start_node_list);


                            String addnl_info_end=output_to_process_end.get(0).toString();
                            addnl_info_end = addnl_info_end.replace("[","");
                            addnl_info_end = addnl_info_end.replace("]","");
                            List<String> addnl_info_list_end = new ArrayList<String>(Arrays.asList(addnl_info_end.split("},")));
                            String addnl_info_value_end =addnl_info_list_end.get(index_number);
                            addnl_info_value_end="["+addnl_info_value_end+"}]";
                            end_node_list.add(addnl_info_value_end);
                            end_node.put(strTemp,end_node_list);

                            continue;
                        }

                        String list_to_process_start=output_to_process_start.get(0).toString();
                        list_to_process_start = list_to_process_start.replace("[","");
                        list_to_process_start = list_to_process_start.replace("]","");
                        List<String> myList_start = new ArrayList<String>(Arrays.asList(list_to_process_start.split(",")));
                        
                        String final_value_start =myList_start.get(index_number);
                        final_value_start="["+final_value_start+"]";
                        
                        start_node_list.add(final_value_start);
                        
                        start_node.put(strTemp,start_node_list);
                        //End Node Processing 

                        
                        String list_to_process_end=output_to_process_end.get(0).toString();
                        list_to_process_end = list_to_process_end.replace("[","");
                        list_to_process_end = list_to_process_end.replace("]","");
                    
                        List<String> myList_end = new ArrayList<String>(Arrays.asList(list_to_process_end.split(",")));
                    
                        
                        String final_value_end =myList_end.get(index_number);
                        final_value_end="["+final_value_end+"]";
                        
                        end_node_list.add(final_value_end);
                        end_node.put(strTemp,end_node_list);

            
                    }
                   
                try {
                    pathResultForCurrentHop.put("\"" + mapKeys.toString() + "\"",
                            mapper.writeValueAsString(thePath.objects()));
                } catch (JsonProcessingException e) {
                    e.printStackTrace();
                }
                //
                counter = counter + 1;
                coveredNodeIds.add(start_v_id);
            }
         

        }
        
        System.gc();
        

    }

    public ResultClassSingleChunkTraversalResults getSingleChunkRootCauseResult() {
        processSingleChunk();

        ResultClassSingleChunkTraversalResults result = new ResultClassSingleChunkTraversalResults(
                pathResultForCurrentHop, intermediateIndexToSearch, sizeCheck, coveredNodeIds);
        return result;

    }

}