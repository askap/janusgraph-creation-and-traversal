package traversal;

import java.io.File;
import java.io.FileWriter;
import java.util.Map;

class JsonPathWritter {

    private String combinedFolderNameWithPath;
    private Map<String, String> pathResultForCurrentHop;
    private int hop;
    private String filename;

    JsonPathWritter(String masterFolderToStoreResult, String levelName, String dateOfInterest
                    ,String filename
                    ,int hop
                    ,Map<String, String> pathResultForCurrentHop){

        this.combinedFolderNameWithPath = masterFolderToStoreResult+"/"+levelName.replace('*', '~')+"_"+dateOfInterest;
        File directory = new File(this.combinedFolderNameWithPath);
        if (! directory.exists()){
            directory.mkdirs();
        }
        this.pathResultForCurrentHop = pathResultForCurrentHop;
        this.hop = hop;
        this.filename = filename;
    }

    private void writingProcess(){

        try{
            FileWriter myWriter = new FileWriter(combinedFolderNameWithPath+"/"+filename+"_"+hop+".json");
            myWriter.write(pathResultForCurrentHop.toString().replace('=', ':'));
            
            myWriter.close();
            System.out.println("Successfully wrote to the file.");  
        } catch (Exception e) {
            System.out.println("Issue in writingProcess function in JsonPathWriter");
            e.printStackTrace();
        }

    }

    public void writePathtoJson(){
        writingProcess();
    }


}