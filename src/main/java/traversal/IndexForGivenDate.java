package traversal;

import java.util.NoSuchElementException;

import org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.GraphTraversalSource;
import org.apache.tinkerpop.gremlin.structure.Vertex;
import org.janusgraph.core.JanusGraph;
import org.janusgraph.core.JanusGraphTransaction;

import utils.SplitStringToListUtil;

class IndexForGivenDate {
    private JanusGraph graph;
    private String dateOfInterest;
    private int dateIndex = -1;
    private String timeColumn;
    private String levelOfInterest;


    IndexForGivenDate(JanusGraph graph, String dateOfInterest, String timeColumn, String levelOfInterest) {
        this.graph = graph;
        this.dateOfInterest = "'"+dateOfInterest+"'";
        this.timeColumn = timeColumn;
        this.levelOfInterest = levelOfInterest;
    }


    private void identifyIndexForGivenDate() {
        JanusGraphTransaction tnx = graph.newTransaction();
        GraphTraversalSource g = tnx.traversal();
        String allDates = "";

        Vertex randomVertexForGivenLevel = null;
        try{
            randomVertexForGivenLevel = g.V().has("level", levelOfInterest).limit(1).next();
            allDates = randomVertexForGivenLevel.value(timeColumn);
        }  catch (NoSuchElementException exception){
            dateIndex = -2;
        } finally{
            // Close the graph traversal g
            try {
                g.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            // Rollback and close the active transaction
            tnx.rollback();
            tnx.close();
        }

        if(dateIndex != -2){
                        
            int idx = 0;
            Boolean indexFoundStatus = false;
            SplitStringToListUtil parseUtils = new SplitStringToListUtil();

            for (String date : parseUtils.parseCommaSeparateparseStringToListOfString(allDates)){
                if(date.equals(dateOfInterest)){
                    indexFoundStatus = true;
                    break;
                }
                else{
                    idx = idx + 1;
                }
            }
            if(indexFoundStatus){
                dateIndex = idx;
            }
        }

    }

    public int getDateIndex(){
        identifyIndexForGivenDate();
        return dateIndex;
    }
    

}