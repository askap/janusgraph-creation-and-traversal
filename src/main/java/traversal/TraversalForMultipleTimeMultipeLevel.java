package traversal;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.validation.constraints.NotNull;

import org.apache.tinkerpop.shaded.jackson.core.JsonParseException;
import org.apache.tinkerpop.shaded.jackson.databind.JsonMappingException;
import org.apache.tinkerpop.shaded.jackson.databind.ObjectMapper;
import org.janusgraph.core.JanusGraph;
import org.janusgraph.core.JanusGraphFactory;

import app.TraversalConcurrentDto;
import utils.YamlFileProcessorUtil;

public class TraversalForMultipleTimeMultipeLevel {

    private String graphLocation ;
    private JanusGraph graph;
    private String timeColumn ;
    private int maxDepthForRootCause ;
    private int maxDepthForImpact ;
    private String folderToWriteResultsRootCause;
    private String folderToWriteResultsImpact;
    private int rangeCheckParam;
    private boolean enablePositiveNegativeRangeCheck;
    private List<String> levelNamesList;
    private List<String> datesList;
    private Double filteringVariableThreshold;
    private String anomaly_experiment_id;
	private String anomaly_run_id;
	private String causal_experiment_id;
	private String causal_run_id;
    private String data_source_id;
    private List<String> requiredAnomalyAlgoFamilies;
    private List<String> historicalAnomalyFamilies;
    private Map<String, List<String>> ruleEngineForRootCauses;
    private List<String> requiredKPIs;
	
    public TraversalForMultipleTimeMultipeLevel(TraversalConcurrentDto traversalConcurrentDto) {
        this.graphLocation = traversalConcurrentDto.getDb_file();
    	this.timeColumn = traversalConcurrentDto.getTime_column();
    	this.maxDepthForRootCause = traversalConcurrentDto.getRoot_cause_max_depth();
        this.maxDepthForImpact =traversalConcurrentDto.getImpact_max_depth();
        this.folderToWriteResultsRootCause = traversalConcurrentDto.getRoot_cause_results_write_folder();
        this.folderToWriteResultsImpact = traversalConcurrentDto.getImpact_results_write_folder();
        this.rangeCheckParam = traversalConcurrentDto.getRange_for_anomaly();
        this.enablePositiveNegativeRangeCheck = false;
        this.levelNamesList = traversalConcurrentDto.getLevelsToTraverse();
        this.datesList = traversalConcurrentDto.getDatesToTraverse();
        this.filteringVariableThreshold = traversalConcurrentDto.getFilteringVariableThreshold();
        this.anomaly_experiment_id = traversalConcurrentDto.getAnomaly_experiment_id();
        this.anomaly_run_id = traversalConcurrentDto.getAnomaly_run_id();
        this.causal_experiment_id = traversalConcurrentDto.getCausal_experiment_id();
        this.causal_run_id = traversalConcurrentDto.getCausal_run_id();
        this.data_source_id = traversalConcurrentDto.getData_source_id();
        this.requiredAnomalyAlgoFamilies = traversalConcurrentDto.getRequiredAnomalyAlgoFamilies();
        this.historicalAnomalyFamilies = new ArrayList<String>();
        this.ruleEngineForRootCauses = new HashMap<String, List<String>>();
        this.requiredKPIs = traversalConcurrentDto.getRequiredKPIs();
    }
    
    private boolean startGraph(){
        try{
            graph = JanusGraphFactory.open(this.graphLocation);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Graph Not Started Successfully");
            return false;
        }
        return true;
    }

    private boolean runParallel(){

        try{

            ExecutorService executor = Executors.newFixedThreadPool(levelNamesList.size());
            
            for(String theDate:datesList){
                for(String levelName:levelNamesList){
                    Runnable worker = new RunnableForTraversalInitiator(graph
                                                                        ,timeColumn,folderToWriteResultsRootCause
                                                                        ,folderToWriteResultsImpact,rangeCheckParam
                                                                        ,enablePositiveNegativeRangeCheck
                                                                        ,maxDepthForRootCause,maxDepthForImpact
                                                                        ,levelName,theDate
                                                                        ,filteringVariableThreshold
                                                                        ,anomaly_experiment_id,anomaly_run_id
                                                                        ,causal_experiment_id,causal_run_id
                                                                        ,data_source_id
                                                                        ,requiredAnomalyAlgoFamilies
                                                                        ,historicalAnomalyFamilies
                                                                        ,ruleEngineForRootCauses
                                                                        ,requiredKPIs);
                    executor.execute(worker);
                }
            }
            executor.shutdown();

            while (!executor.isTerminated()) {
            }
            
            System.out.println("\nFinished all threads");

            return true;
        } catch (Exception e) {
            System.out.println("Issue with runParallel function in TraversalForMultipleTimeMultipeLevel");
            return false;
        }
    }

    private boolean runLinear() {
        try{

            for(String theDate:datesList){
                for(String levelName:levelNamesList){
                    System.out.println(theDate+" :: "+levelName);
                    TraversalInitiatorForOneTimeOneLevel linearObj = 
                        new TraversalInitiatorForOneTimeOneLevel(graph, timeColumn
                                                                ,folderToWriteResultsRootCause
                                                                ,folderToWriteResultsImpact
                                                                ,rangeCheckParam
                                                                ,enablePositiveNegativeRangeCheck
                                                                ,maxDepthForRootCause
                                                                ,maxDepthForImpact
                                                                ,levelName
                                                                ,theDate
                                                                ,filteringVariableThreshold
                                                                ,anomaly_experiment_id
                                                                ,anomaly_run_id,causal_experiment_id
                                                                ,causal_run_id,data_source_id
                                                                ,requiredAnomalyAlgoFamilies
                                                                , historicalAnomalyFamilies
                                                                ,ruleEngineForRootCauses
                                                                ,requiredKPIs);
                    linearObj.runTraversal();
                }
            }
            
            System.out.println("\nFinished all runs");

            return true;

        } catch (Exception e) {
            System.out.println("Issue with runLinear function in TraversalForMultipleTimeMultipeLevel");
            return false;
        }
    }

    public boolean initiateTraversal(boolean parallelFlag){
        try{
                ObjectMapper mapper = new ObjectMapper();
        
                
                YamlFileProcessorUtil yamlObj = new YamlFileProcessorUtil("/graph_config.yaml");
                
                historicalAnomalyFamilies = yamlObj.getHistoricalAnomalyFamilies();
                  
                ruleEngineForRootCauses = yamlObj.getRuleEngineForRootCauses();

                boolean graphStartStatus = startGraph();
                if(graphStartStatus == false){
                    System.out.println("graphStartStatus is false");
                    return false;
                }
                if(parallelFlag){
                    runParallel();
                } else {
                    runLinear();
                }
                return true;
            } catch (Exception e){
                e.printStackTrace();
                System.out.println("Issue with initiateTraversal function in TraversalForMultipleTimeMultipeLevel");
                return false;
            } finally {
                graph.close();
            }
    }
    
}