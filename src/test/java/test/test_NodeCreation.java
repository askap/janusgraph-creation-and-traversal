package test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import com.univocity.parsers.common.record.Record;
import com.univocity.parsers.csv.CsvParser;

import org.apache.commons.collections.ListUtils;
import org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.GraphTraversalSource;
import org.janusgraph.core.JanusGraph;
import org.janusgraph.core.JanusGraphFactory;
import org.janusgraph.core.JanusGraphTransaction;
import org.janusgraph.core.JanusGraphVertex;

import utils.CsvFileProcessingUtil;
import com.univocity.parsers.common.processor.RowListProcessor;
import com.univocity.parsers.common.record.Record;
import com.univocity.parsers.csv.*;


import graphcreation.NodeCreation;

import static org.junit.Assert.assertEquals;  
import org.junit.After;  
import org.junit.AfterClass;  
import org.junit.Before;  
import org.junit.BeforeClass;  
import org.junit.Test;  

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import org.yaml.snakeyaml.Yaml;





public class test_NodeCreation {
    /*
 * 	This module is for unit testing NodeCreation module.
 *  This module has a local .yaml file and local_db where the graph will be created and will refer this graph for checking our result.
 
 *  Here two objects are created one for local_db and other for referenceing the test_db 
   
   	Attributes
   	----------
   	yaml_file : .yaml file
       Configuration file which contains all the 
       properties of a node , edge and all the other 
       data files and graph database location 
    
    data_file : .txt 
     	Contains the data to load into the graph.
       
   	Examples
   	--------
   	The loadData function will be called from 
    a separate wrapper as an object of loadingData class	:
   	
   	obj = loadData("data_file","test.yaml");
*/
private long BATCH_SIZE;
private String db_file;
protected static JanusGraph graph,graph_test;

public static Reader getReader(final String relativePath) {

    Reader in = null;
    try {
        in = new InputStreamReader(new FileInputStream(relativePath));
    } catch (final FileNotFoundException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
    }
    return in;

}

public static void setUpBeforeClass() throws Exception {
    System.out.println("before class");
}

public void testing_NodeCreation(final String fileName,final String yamlFile,final String test_db ) {
    final HashMap<String,Class> MAP=new HashMap<String,Class>();
	MAP.put("String", String.class);
    MAP.put("Character", Character.class);
    MAP.put("Boolean", Boolean.class);
    MAP.put("Byte", Byte.class);
    MAP.put("Short", Short.class);
    MAP.put("Integer", Integer.class);
    MAP.put("Long", Long.class);
    MAP.put("Float", Float.class);
    MAP.put("UUID", UUID.class);
    MAP.put("Date", Date.class);
    
    
 // Object to pass the YAML file
	Map yamlMap = null;
	// YAML OBJECT 
	final Yaml yaml = new Yaml();
	try {
        final InputStream stream =new FileInputStream(yamlFile);
        yamlMap = (Map) yaml.load(stream);
        BATCH_SIZE=(Long) yamlMap.get("batch_commit_size");
        db_file=(String) yamlMap.get("db_file");
        graph = JanusGraphFactory.open(db_file);
        graph_test=JanusGraphFactory.open(test_db);
        JanusGraphTransaction tnx = graph.newTransaction();
        JanusGraphTransaction test_tnx = graph_test.newTransaction();
    
        CsvParserSettings settings = new CsvParserSettings();
        RowListProcessor rowProcessor = new RowListProcessor();
        settings.getFormat().setDelimiter("|");
        settings.setProcessor(rowProcessor);
        settings.setHeaderExtractionEnabled(true);
        CsvParser parser = new CsvParser(settings);
    
        parser.beginParsing(getReader(fileName));
        parser.parse(getReader(fileName));
        
        //getting column names from a CSV file 
        String[] headers = rowProcessor.getHeaders();
        List<String> header_list = new ArrayList<String>(Arrays.asList(headers));
        header_list.remove("key");
    
        
        ArrayList<String> node_key_columns = new ArrayList<String>();
        node_key_columns.addAll((Collection<? extends String>) yamlMap.get("NODE_KEY_COLUMNS"));
        
        List node_properties = ListUtils.subtract(header_list, node_key_columns);
        
        
        int i = 0;
        Record record;
        
        while ((record = parser.parseNextRecord()) != null) {
            i++;
            //loading the data of the key columns manually as it has to be present 
            
             String key = record.getString("key");
             String kpi = record.getString("kpi");
             String level = record.getString("level");
             String time = "[" + record.getString("time") + "]";
             String level_num = record.getString("level_num");
             int level_number=Integer.parseInt(level_num);
            //property created by us 
             String id = kpi + "-*-" + key;
            
             JanusGraphVertex v = tnx.addVertex("KPI");
            v.property("name", kpi);
            v.property("ID", id);
            v.property("level", level);
            v.property("time", time);
            v.property("level_num", level_number);
            for (int j = 0; j < node_properties.size();j++) {
                final String property=(String) node_properties.get(j);
                final String informationFromfile = "[" + record.getString(property) + "]";
                v.property(property, informationFromfile);
            }
            if (i % BATCH_SIZE == 0) {
                tnx.commit();
                tnx.close();
                tnx = graph.newTransaction();
                System.out.println("periodic commit at : " + i);
            }
           
        }

        parser.stopParsing();
        if (tnx.isOpen()) {
            tnx.commit();
            tnx.close();
            System.out.println("Final commit at : " + i);	
    
        }
        // Checking if no. of nodes created for both local and reference graph is equal or not. 
        tnx = graph.newTransaction();
        GraphTraversalSource g = tnx.traversal();
        System.out.print("local GraphTraversal object Created \n");

        List<Long> local_nodes = g.V().count().toList();


        GraphTraversalSource g_test = test_tnx.traversal();
        System.out.print("Reference GraphTraversal object Created \n");
        List<Long> reference_nodes = g_test.V().count().toList();

        assertEquals(local_nodes,reference_nodes);

        // Random Node Check
        List<Map<Object, Object>> local_node1 = g.V().has("ID", "dollar_sales-*-Overall").valueMap().toList();
        List<Map<Object, Object>> reference_node1 = g_test.V().has("ID", "dollar_sales-*-Overall").valueMap().toList();

        assertEquals(local_node1,reference_node1);

        if (tnx.isOpen()) {
            test_tnx.close();
            tnx.close();
            graph.close();
            graph_test.close();	
    
        }

        
        }
        catch (final FileNotFoundException e) {
            System.out.println("No such file " + yamlFile);
            }

}

    
}